/**
 * @providesModule registerForPushNotificationsAsync
 * @flow
 */

import { Permissions, Notifications } from 'exponent'

export default async function registerForPushNotificationsAsync (auth_token, user_id) {
  let { status } = await Permissions.askAsync(Permissions.REMOTE_NOTIFICATIONS)

  if (status !== 'granted') {
    return
  }

  let token = await Notifications.getExponentPushTokenAsync()

  return fetch('https://graphql.beefup.eu/expo/pushtoken', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': auth_token
    },
    body: JSON.stringify({
      token,
      user_id
    })
  }).catch(err => {
    alert('Cannot subscribe for notifications! Please restart the app...')
    console.error(err)
  })
}
