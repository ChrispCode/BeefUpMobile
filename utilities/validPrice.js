export default function validPrice(itemPrice) {
    if (itemPrice === 0) {
      return '0,00 lv'
    }

    itemPrice = itemPrice.toString()
    let price = `${itemPrice.substring(0, itemPrice.length - 2)},${itemPrice.split("").reverse().join("").substring(0, 2)} lv`

    return price;
}
