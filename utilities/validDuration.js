export default function validDuration (duration) {
  let hours = Math.floor(duration / 60)
  let minutes = duration % 60
  hours = hours > 0 ? `${hours}h` : ''
  minutes = minutes > 0 ? `${minutes}mins` : ''
  return `${hours} ${minutes}`
}
