/**
 * @providesModule main
 * @flow
 */
import Exponent, { Components } from 'exponent'
import React from 'react'
import {
  ActivityIndicator,
  Platform,
  StatusBar,
  StyleSheet,
  View,
  NetInfo,
  Permissions,
  Notifications,
  Alert
} from 'react-native'
import { withNavigation, NavigationProvider, StackNavigation } from '@exponent/ex-navigation'
import { MaterialIcons, Ionicons } from '@exponent/vector-icons'
import { connect } from 'react-redux'
import BeefUpStore, { client } from 'BeefUpStore'
import { RegularText } from 'StyledText';
import Actions from 'Actions'
import Colors from 'Colors'
import Layout from 'Layout'
import LocalStorage from 'LocalStorage'
import Router from 'Router'
import { cacheFonts } from './utilities/cacheHelpers'
import { ApolloProvider } from 'react-apollo'

class AppContainer extends React.Component {
  render () {
    return (
      <ApolloProvider store={BeefUpStore} client={client}>
        <NavigationProvider router={Router}>
          <App {...this.props} />
        </NavigationProvider>
      </ApolloProvider>
    )
  }
}

@withNavigation
@connect(data => App.getDataProps)
class App extends React.Component {
  static getDataProps (data) {
    return {
      currentUser: data.currentUser,
      isGlobalLoadingVisible: data.global.isLoading,
    }
  };

  state = {
    bootstrapIsComplete: false
  };

  componentWillMount () {
    this._bootstrap()
  }

  componentDidMount () {
    NetInfo.addEventListener(
      'change',
      this._handleConnectionInfoChange
    )
  }

  _handleConnectionInfoChange = (connectionInfo) => {
    if (connectionInfo === 'NONE') {
      this.setState({
        networkError: true
      })
    } else {
      this.setState({
        networkError: false
      })
    }
  }

  componentWillUnmount () {
    NetInfo.removeEventListener(
      'change',
      this._handleConnectionInfoChange
    )
  }

  componentDidUpdate (prevProps) {
    if (!this.state.bootstrapIsComplete) {
      return;
    }

    const rootNavigator = this.props.navigation.getNavigator('root')

    if (!isLoggedIn(prevProps.currentUser) && isLoggedIn(this.props.currentUser)) {
      rootNavigator.replace(Router.getRoute(Layout.navigationLayoutRoute))
    } else if (isLoggedIn(prevProps.currentUser) && !isLoggedIn(this.props.currentUser)) {
      rootNavigator.replace(Router.getRoute('authentication'))
    }
  }

  render() {
    if (!this.state.bootstrapIsComplete) {
      return <Components.AppLoading />
    }

    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle='light-content' />}
        {Platform.OS === 'android' && <View style={styles.statusBarUnderlay} />}

        <StackNavigation
          id='root'
          initialRoute={getInitialRoute(this.props.currentUser)}
        />

        {this.state.networkError && this._renderNetworkErrorOverlay()}
        {this.props.isGlobalLoadingVisible && this._renderGlobalLoadingOverlay()}
      </View>
    )
  }

  _renderNetworkErrorOverlay = () => {
    return (
      <View style={[StyleSheet.absoluteFill, styles.networkOverlay]}>
        <MaterialIcons name='warning' size={72} color='#ff0000' />
        <RegularText style={{fontSize: 18}}>Please check your internet connection</RegularText>
      </View>
    )
  }

  _renderGlobalLoadingOverlay = () => {
    return (
      <View style={[StyleSheet.absoluteFill, styles.loadingOverlay]}>
        <ActivityIndicator color={Colors.midGrey}/>
      </View>
    )
  }

  async _bootstrap() {
    try {
      let fetchUser = LocalStorage.getUserAsync();

      let fontAssets = cacheFonts([
        Platform.OS === 'ios' ? Ionicons.font : MaterialIcons.font,
        { 'open-sans-light': require('./assets/fonts/opensans-light.ttf') },
        { 'open-sans': require('./assets/fonts/opensans-regular.ttf') },
        { 'open-sans-bold': require('./assets/fonts/opensans-bold.ttf') }
      ])

      let [user] = await Promise.all([
        fetchUser,
        ...fontAssets
      ])

      user && this.props.dispatch(Actions.signIn(user))
      this.setState({ bootstrapIsComplete: true })
    } catch (e) {
      Alert.alert('Error on bootstrap!', e.message)
    }
  }
}

function isLoggedIn (userState) {
  return !!userState.token
}

function getInitialRoute(currentUser) {
  if (isLoggedIn(currentUser)) {
    return Router.getRoute(Layout.navigationLayoutRoute)
  } else {
    return Router.getRoute('authentication')
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBarUnderlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 25,
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  loadingOverlay: {
    backgroundColor: 'rgba(255,255,255,0.5)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  networkOverlay: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

Exponent.registerRootComponent(AppContainer)
