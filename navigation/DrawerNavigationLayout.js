/**
 * @providesModule DrawerNavigationLayout
 * @flow
 */
import React from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { StackNavigation, DrawerNavigation, DrawerNavigationItem } from '@exponent/ex-navigation'
import { MaterialIcons } from '@exponent/vector-icons'
import { connect } from 'react-redux'
import Actions from 'Actions'
import Colors from 'Colors'
import Router from 'Router'
import { Font } from 'exponent'
import { RegularText } from 'StyledText'
import registerForPushNotificationsAsync from 'registerForPushNotificationsAsync'

const defaultRouteConfig = {
  navigationBar: {
    tintColor: Colors.navigationBarTintColor,
    backgroundColor: Colors.navigationBarBackgroundColor,
    titleStyle: Font.style('open-sans')
  }
}

@connect(data => DrawerNavigationLayout.getDataProps)
export default class DrawerNavigationLayout extends React.Component {
  static getDataProps (data) {
    return {
      currentUser: data.currentUser
    }
  }

  componentWillMount () {
    registerForPushNotificationsAsync(this.props.currentUser.token, this.props.currentUser.id)
  }

  render () {
    if (this.props.currentUser.role === 'normal') {
      return (
        <DrawerNavigation
          renderHeader={this._renderHeader}
          drawerWidth={300}
          initialItem='restaurants'>

          <DrawerNavigationItem
            id='restaurants'
            selectedStyle={styles.selectedItemStyle}
            renderTitle={isSelected => this._renderTitle('Restaurants', isSelected)}
            renderIcon={isSelected => this._renderIcon('restaurant', isSelected)}>
            <StackNavigation
              defaultRouteConfig={defaultRouteConfig}
              initialRoute={Router.getRoute('restaurants')}
            />
          </DrawerNavigationItem>
          <DrawerNavigationItem
            id='orders'
            selectedStyle={styles.selectedItemStyle}
            renderTitle={isSelected => this._renderTitle('Orders', isSelected)}
            renderIcon={isSelected => this._renderIcon('assignment', isSelected)}>
            <StackNavigation
              defaultRouteConfig={defaultRouteConfig}
              initialRoute={Router.getRoute('orders')}
            />
          </DrawerNavigationItem>
          <DrawerNavigationItem
            id='reservations'
            selectedStyle={styles.selectedItemStyle}
            renderTitle={isSelected => this._renderTitle('Reservations', isSelected)}
            renderIcon={isSelected => this._renderIcon('event', isSelected)}>
            <StackNavigation
              defaultRouteConfig={defaultRouteConfig}
              initialRoute={Router.getRoute('reservations')}
            />
          </DrawerNavigationItem>
          <DrawerNavigationItem
            id='profile'
            selectedStyle={styles.selectedItemStyle}
            renderTitle={isSelected => this._renderTitle('Profile', isSelected)}
            renderIcon={isSelected => this._renderIcon('face', isSelected)}>
              <StackNavigation
                defaultRouteConfig={defaultRouteConfig}
                initialRoute={Router.getRoute('profile')}
              />
          </DrawerNavigationItem>
          <DrawerNavigationItem
            id='sign-out'
            selectedStyle={styles.selectedItemStyle}
            onPress={() => this.props.dispatch(Actions.signOut())}
            renderTitle={isSelected => this._renderTitle('Sign out', isSelected)}
            renderIcon={isSelected => this._renderIcon('exit-to-app', isSelected)}>
            <View />
          </DrawerNavigationItem>
        </DrawerNavigation>
      )
    }

    return (
      <DrawerNavigation
        renderHeader={this._renderHeader}
        drawerWidth={300}
        initialItem='orders'>

        <DrawerNavigationItem
          id='orders'
          selectedStyle={styles.selectedItemStyle}
          renderTitle={isSelected => this._renderTitle('Orders', isSelected)}
          renderIcon={isSelected => this._renderIcon('assignment', isSelected)}>
          <StackNavigation
            defaultRouteConfig={defaultRouteConfig}
            initialRoute={Router.getRoute('orders')}
          />
        </DrawerNavigationItem>
        <DrawerNavigationItem
          id='sign-out'
          selectedStyle={styles.selectedItemStyle}
          onPress={() => this.props.dispatch(Actions.signOut())}
          renderTitle={isSelected => this._renderTitle('Sign out', isSelected)}
          renderIcon={isSelected => this._renderIcon('exit-to-app', isSelected)}>
          <View />
        </DrawerNavigationItem>
      </DrawerNavigation>
    )
  }

  _renderHeader = () => {
    return (
      <View style={{
        height: 125,
        paddingBottom: 12,
        paddingLeft: 12,
        backgroundColor: Colors.tintColor,
        justifyContent: 'flex-end'
      }}>
        <RegularText style={{ fontSize: 36, color: '#fff' }}>
          BeefUp
        </RegularText>
      </View>
    )
  };

  _renderTitle (text, isSelected) {
    return (
      <Text style={[styles.buttonTitleText, isSelected ? styles.buttonTitleTextSelected : {}]}>
        {text}
      </Text>
    )
  }

  _renderIcon (name, isSelected) {
    return (
      <View style={{ width: 28 }}>
        <MaterialIcons
          name={name}
          size={28}
          color={isSelected ? Colors.drawerIconSelected : Colors.drawerIconDefault}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  buttonTitleText: {
    color: Colors.drawerTextDefault,
    fontWeight: 'bold',
    marginLeft: 18
  },
  buttonTitleTextSelected: {
    color: Colors.tintColor
  },
  selectedItemStyle: {
    backgroundColor: '#EBEBEB'
  }
})
