/**
 * @providesModule TabNavigationLayout
 * @flow
 */
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { StackNavigation, TabNavigation, TabNavigationItem } from '@exponent/ex-navigation'
import { Ionicons } from '@exponent/vector-icons'
import { Font } from 'exponent'
import Colors from 'Colors'
import Router from 'Router'

const defaultRouteConfig = {
  navigationBar: {
    tintColor: Colors.navigationBarTintColor,
    backgroundColor: Colors.navigationBarBackgroundColor,
    titleStyle: Font.style('open-sans')
  }
}

export default class TabNavigationLayout extends React.Component {
  render () {
    return (
      <TabNavigation
        tabBarColor={Colors.tabBar}
        tabBarHeight={56}
        initialTab='restaurants'>
        <TabNavigationItem
          id='restaurants'
          renderIcon={isSelected => this._renderIcon('Restaurants', 'ios-pizza-outline', isSelected)}>
          <StackNavigation
            defaultRouteConfig={defaultRouteConfig}
            initialRoute={Router.getRoute('restaurants')}
          />
        </TabNavigationItem>
      </TabNavigation>
    )
  }

  _renderIcon (title, iconName, isSelected) {
    let color = isSelected ? Colors.tabIconSelected : Colors.tabIconDefault

    return (
      <View style={styles.tabItemContainer}>
        <Ionicons name={iconName} size={32} color={color} />

        <Text style={[styles.tabTitleText, { color }]} numberOfLines={1}>
          {title}
        </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  tabItemContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitleText: {
    fontSize: 11
  }
})
