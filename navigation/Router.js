/**
 * @providesModule Router
 * @flow
 */
import { createRouter } from '@exponent/ex-navigation'
import OrdersScreen from 'OrdersScreen'
import OrderScreen from 'OrderScreen'
import PayOrderScreen from 'PayOrderScreen'
import EditOrderScreen from 'EditOrderScreen'
import ReservationsScreen from 'ReservationsScreen'
import ReservationScreen from 'ReservationScreen'
import CreateReservationScreen from 'CreateReservationScreen'
import RestaurantsScreen from 'RestaurantsScreen'
import RestaurantScreen from 'RestaurantScreen'
import DrawerNavigationLayout from 'DrawerNavigationLayout'
import TabNavigationLayout from 'TabNavigationLayout'
import AuthenticationScreen from 'AuthenticationScreen'
import ProfileScreen from 'ProfileScreen'

export default createRouter(() => ({
  orders: () => OrdersScreen,
  order: () => OrderScreen,
  payOrder: () => PayOrderScreen,
  editOrder: () => EditOrderScreen,
  reservations: () => ReservationsScreen,
  reservation: () => ReservationScreen,
  createReservation: () => CreateReservationScreen,
  restaurants: () => RestaurantsScreen,
  restaurant: () => RestaurantScreen,
  drawerNavigationLayout: () => DrawerNavigationLayout,
  tabNavigationLayout: () => TabNavigationLayout,
  authentication: () => AuthenticationScreen,
  profile: () => ProfileScreen
}), {
  ignoreSerializableWarnings: true
})
