/**
 * @providesModule RestaurantScreen
 * @flow
 */
import React from 'react'
import {
  ScrollView,
  StyleSheet,
  View,
  TouchableOpacity,
  Modal,
  ActivityIndicator
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigation } from '@exponent/ex-navigation'
import TouchableNativeFeedbackSafe from '@exponent/react-native-touchable-native-feedback-safe'
import QRCode from 'react-native-qrcode'
import Router from 'Router'
import Colors from 'Colors'
import Actions from 'Actions'
import { BoldText, RegularText, LightText } from 'StyledText'
import StyledTextInput from 'StyledTextInput'
import ItemListView from 'ItemListView'
import { graphql, compose } from 'react-apollo'
import MapView from 'react-native-maps'
import { Components } from 'exponent'
import { MaterialIcons } from '@exponent/vector-icons'
import gql from 'graphql-tag'

@withNavigation
class CreateReservation extends React.Component {
  render () {
    return (
      <TouchableOpacity
        style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginRight: 15 }}
        onPress={() => this._createReservation()}>
        <MaterialIcons name='event' size={28} color='#000' />
      </TouchableOpacity>
    )
  }

  _createReservation () {
    console.log(this.props)
    this.props.navigator.push(Router.getRoute('createReservation', {
      restaurant: this.props.restaurant
    }))
  }
}

const query = gql`
    query($id: Int!) {
        restaurant(id: $id) {
            street_name
            street_number
            city
            country
            latitude
            longitude
            items {
                name
                price
                liquid
            }
        }
    }
`

const mutation = gql`
    mutation ($items: [OrderItemInput]!) {
        createOrder(items: $items) {
            id
        }
    }
`

@connect(data => RestaurantScreen.getDataProps)
@compose(
  graphql(query, {
    name: 'restaurantData',
    options: (ownProps) => ({
      variables: {
        id: ownProps.restaurant.id
      }
    })
  }),
  graphql(mutation, {
    props: ({ ownProps, mutate }) => ({
      createOrder: ({ items }) => {
        return new Promise(resolve => {
          return resolve(mutate({
            variables: {
              items
            }
          }))
        })
      }
    })
  })
)
export default class RestaurantScreen extends React.Component {
  constructor (props, context) {
    super(props, context)

    this.state = {
      itemFilter: '',
      restaurant: null,
      filteredItems: null,
      requestedToEnter: false,
      loading: true,
      channel: null
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      restaurant: nextProps.restaurantData.restaurant,
      loading: nextProps.restaurantData.loading
    })

    if (nextProps.restaurantData.restaurant) {
      this.setState({
        filteredItems: nextProps.restaurantData.restaurant.items
      })
    }
  }

  componentDidMount () {
    let channelName = `private-restaurants-client-${this.props.currentUser.id}`
    let channel = this.props.pusher.subscribe(channelName)
    this.setState({
      channel
    })
    channel.bind('auth_client', data => {
      this.setState({
        requestedToEnter: false
      })
      this.props.dispatch(Actions.authorizeUserInRestaurant(this.props.restaurant.id))
      let rootNavigation = this.props.navigation.getNavigator('root')
      rootNavigation.push(Router.getRoute('order', {restaurant_id: this.props.restaurant.id}))
    })
  }

  componentWillUnmount () {
    this.state.channel.unsubscribe()
  }

  static getDataProps (data) {
    return {
      currentUser: data.currentUser,
      pusher: data.pusher
    }
  }

  static route = {
    navigationBar: {
      title ({ restaurant }) {
        return restaurant.name
      },
      renderRight ({ params }) {
        return <CreateReservation restaurant={params.restaurant} />
      }
    }
  }

  render () {
    if (this.state.loading) {
      return (
        <View style={styles.loadingContainer}>
          <ActivityIndicator color={Colors.midGrey} />
          <LightText>Loading...</LightText>
        </View>
      )
    }

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        automaticallyAdjustContentInsets={false}>
        <Components.MapView
          initialRegion={{
            latitude: this.props.restaurantData.restaurant.latitude,
            longitude: this.props.restaurantData.restaurant.longitude,
            latitudeDelta: 0.01,
            longitudeDelta: 0.1
          }}
          style={styles.map}
        >
          <MapView.Marker
          coordinate={{
            latitude: this.props.restaurantData.restaurant.latitude,
            longitude: this.props.restaurantData.restaurant.longitude,
            latitudeDelta: 0.5,
            longitudeDelta: 0
          }} />
        </Components.MapView>

        <BoldText style={styles.title}>Location</BoldText>

        {this._renderLocation(this.state.restaurant)}

        <BoldText style={styles.title}>
          Items
        </BoldText>
        <StyledTextInput
          autoCorrect={false}
          autoCapitalize='none'
          blurOnSubmit={false}
          onChangeText={value => {
            this.setState({ itemFilter: value })
            this._handleFilterItems(value)
          }}
          onSubmitEditing={this._handleSubmitFilter}
          value={this.state.itemFilter}
          keyboardType='default'
          placeholder='Search'
          style={{
            color: '#000'
          }}
        />
        <ItemListView items={this.state.filteredItems} />
        <View style={{ marginTop: 10 }}>
          <TouchableNativeFeedbackSafe
            background={TouchableNativeFeedbackSafe.Ripple()}
            delayPressIn={0}
            onPress={this._handleEnter}
            style={styles.enterButton}>
            <RegularText style={styles.enterText}>Enter</RegularText>
          </TouchableNativeFeedbackSafe>
        </View>

        {this._renderModal(this.props.restaurant.id, this.props.currentUser.id)}
      </ScrollView>
    )
  }

  _renderModal(restaurant_id, user_id) {
    if (!!restaurant_id && !!user_id) {
      let barcode = JSON.stringify({
        restaurant_id: restaurant_id,
        client_id: user_id
      })
      return (
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.requestedToEnter}
          onRequestClose={this._handleEnter}>
          <View style={styles.enterModal}>
            <View />
            <QRCode
              value={barcode}
              size={200}
              bgColor={Colors.tintColor}
              fgColor='#fff' />
            <View style={{ marginTop: 10 }}>
              <RegularText style={styles.closeText} onPress={this._handleEnter}>Cancel</RegularText>
            </View>
          </View>
        </Modal>
      )
    }
  }

  _renderLocation (restaurant) {
    let location = restaurant.street_name
      ? `${restaurant.street_name}${restaurant.street_number ? ' ' +
        restaurant.street_number : ''}, ${restaurant.city}, ${restaurant.country}`
      : `${restaurant.city}, ${restaurant.country}`

    return (
      <RegularText style={styles.text}>{location}</RegularText>
    )
  }

  _handleEnter = () => {
    this.setState({
      requestedToEnter: !this.state.requestedToEnter
    })
  }

  _handleFilterItems = (filter) => {
    filter = filter.toLowerCase()
    let filteredItems = this.state.restaurant.items.filter(item => item.name.toLowerCase().indexOf(filter) > -1)
    this.setState({
      filteredItems: filteredItems
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  map: {
    flex: 1,
    height: 200,
    maxHeight: 200,
    marginBottom: 10
  },
  loadingContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  enterModal: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 50
  },
  contentContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15
  },
  title: {
    fontSize: 16,
    color: Colors.darkGrey,
    marginBottom: 10
  },
  text: {
    fontSize: 13,
    color: Colors.midGrey,
    marginBottom: 20
  },
  enterText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: '#f7f5f5'
  },
  enterButton: {
    paddingVertical: 10,
    borderRadius: 2,
    backgroundColor: Colors.tintColor,
    alignItems: 'center',
    justifyContent: 'center'
  },
  closeText: {
    fontSize: 18,
    color: Colors.tintColor
  }
})
