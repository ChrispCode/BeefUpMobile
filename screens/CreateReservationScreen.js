/**
 * @providesModule CreateReservationScreen
 * @flow
 */
import React from 'react'
import {
  ScrollView,
  Switch,
  StyleSheet,
  DatePickerAndroid,
  TimePickerAndroid,
  View,
  Modal,
  Picker,
  ActivityIndicator,
  ToastAndroid,
  TouchableWithoutFeedback
} from 'react-native'
import Colors from 'Colors'
import Alerts from 'Alerts'
import TouchableNativeFeedbackSafe from '@exponent/react-native-touchable-native-feedback-safe'
import { BoldText, RegularText } from 'StyledText'
import { graphql, compose } from 'react-apollo'
import Router from 'Router'
import validPrice from '../utilities/validPrice'
import validDate from '../utilities/validDate'
import validDuration from '../utilities/validDuration'
import { withNavigation } from '@exponent/ex-navigation'
import CardsListView from 'CardsListView'
import StyledTextInput from 'StyledTextInput'
import gql from 'graphql-tag'
const Item = Picker.Item

const query = gql`
    query {
        user {
            cards {
                id
                last4
                scheme
            }
        }
    }
`

const mutation = gql`
    mutation($restaurant_id: Int!, $date: Date!, $number_of_people: Int!, $duration: Int) {
        createReservation(restaurant_id: $restaurant_id, date: $date, number_of_people: $number_of_people,
        duration: $duration) {
            id
        }
    }
`

const mutationPreorder = gql`
    mutation($restaurant_id: Int!, $date: Date!, $number_of_people: Int!, $duration: Int, $items: [OrderItemInput]!,
    $card_id: Int!) {
        createReservationPreorder(restaurant_id: $restaurant_id, date: $date, number_of_people: $number_of_people,
        duration: $duration, items: $items, card_id: $card_id) {
            id
        }
    }
`

@compose(
  graphql(query, {
    name: 'userData'
  }),
  graphql(mutation, {
    props: ({ ownProps, mutate }) => ({
      createReservation: ({ restaurant_id, date, number_of_people, duration }) => {
        return new Promise(resolve => {
          return resolve(mutate({
            variables: {
              restaurant_id,
              date,
              number_of_people,
              duration
            }
          }))
        })
      }
    })
  }),
  graphql(mutationPreorder, {
    props: ({ ownProps, mutate }) => ({
      createReservationPreorder: ({ restaurant_id, date, number_of_people, duration, items, card_id }) => {
        return new Promise(resolve => {
          return resolve(mutate({
            variables: {
              restaurant_id,
              date,
              number_of_people,
              duration,
              items,
              card_id
            }
          }))
        })
      }
    })
  })
)
@withNavigation
export default class CreateReservationScreen extends React.Component {
  state = {
    isRequestInFlight: false,
    time: {},
    duration: 30,
    preOrder: false,
    user: {},
    items: [],
    orderItems: new Map(),
    value: 0,
    requestedToEnter: false
  }

  static route = {
    navigationBar: {
      title ({ restaurant }) {
        return `Make reservation - ${restaurant.name}`
      }
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      user: nextProps.userData.user,
      loading: nextProps.userData.loading
    })
  }

  _showDatePicker = async (stateKey, options) => {
    if (options.date) {
      options.date.setDate(options.date.getDate() + 1)
    }
    try {
      const { action, year, month, day } = await DatePickerAndroid.open(options)
      if (action !== DatePickerAndroid.dismissedAction) {
        let date = new Date(year, month, day)
        this.setState({
          date
        })
      }
    } catch ({ code, message }) {
      alert('Cannot select date')
    }
  }

  _showTimePicker = async (stateKey, options) => {
    try {
      const { action, minute, hour } = await TimePickerAndroid.open(options)
      if (action === TimePickerAndroid.timeSetAction) {
        this.setState({
          time: {
            minute,
            hour
          }
        })
      }
    } catch ({ code, message }) {
      alert('Cannot select time')
    }
  };

  render () {
    let durations = Array.from(Array(16), (_, x) => (x + 1) * 15)
    let timeItems = durations.map(minutes => {
      return (<Item key={minutes} label={`${validDuration(minutes)}`} value={minutes} />)
    })

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        automaticallyAdjustContentInsets={false}>
        <TouchableWithoutFeedback
          onPress={() => this._showDatePicker('date', {
            date: this.state.date,
            minDate: new Date()
          })}>
          <View style={styles.prop}><BoldText>Select date</BoldText><RegularText>{this.state.date ? ` - ${validDate(this.state.date)}` : ''}</RegularText></View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => this._showTimePicker('preset', {
            hour: this.state.time.hour || new Date().getHours(),
            minute: this.state.time.minute || new Date().getMinutes(),
            is24Hour: true
          })}>
          <View style={styles.prop}><BoldText>Select hours</BoldText><RegularText>{this.state.time.hour ? ` - ${this.state.time.hour}:${this.state.time.minute}` : ''}</RegularText></View>
        </TouchableWithoutFeedback>
        <View style={styles.prop}>
          <BoldText>People </BoldText>
          <StyledTextInput
            autoCorrect={false}
            autoCapitalize='none'
            blurOnSubmit={false}
            value={this.state.number_of_people}
            onChangeText={value => {
              this.setState({number_of_people: value})
            }}
            keyboardType='numeric'
            style={{
              height: 15,
              fontSize: 12,
              color: '#000'
            }}
          />
        </View>
        <View style={styles.prop}>
          <BoldText>Pre order</BoldText>
          <Switch
            disabled={this.state.loading}
            onValueChange={value => this.setState({ preOrder: value })}
            value={this.state.preOrder}
          />
        </View>
        {this.state.preOrder &&
          <View>
            {this._renderItems()}
            {this._renderCards()}
          </View>
        }

        <BoldText>Estimated duration </BoldText>
        <Picker
          selectedValue={this.state.duration}
          onValueChange={value => this.setState({ duration: value })}
          mode='dialog'>
          {timeItems}
        </Picker>
        <View style={{ marginTop: 10 }}>
          {this._renderMakeReservation()}
        </View>
      </ScrollView>
    )
  }

  _renderItems () {
    return (
      <TouchableWithoutFeedback
        onPress={() => this._handleEnterItems()} >
        <View style={[styles.prop, { justifyContent: 'space-between' }]}>
          <RegularText>Total: {validPrice(this.state.value)}</RegularText>
          <RegularText>{this.state.items.length} items</RegularText>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  _handleEnterItems () {
    this.props.navigator.push(Router.getRoute('order', {
      restaurant_id: this.props.restaurant.id,
      reservation: true,
      buttonText: 'Done',
      onOrder: this._handleItems.bind(this),
      items: this.state.orderItems.size > 0 ? this.state.orderItems : null,
      value: this.state.value
    }))
  }

  _handleItems (orderItems, items, value) {
    console.log(orderItems, items, value)
    this.setState({
      orderItems,
      items,
      value
    })
  }

  _renderCards () {
    return (
      <CardsListView
        cards={this.state.user.cards}
        onSelectCard={(card) => this._handleSelectCard(card)}
        selectedCardId={this.state.selectedCardId}
      />
    )
  }

  _handleSelectCard (card) {
    this.setState({
      selectedCardId: card.id
    })
  }

  _handleMake (restaurant_id, date, number_of_people, duration, items, card_id) {
    this.setState({
      isRequestInFlight: true
    })
    if (this.state.preOrder) {
      this.props.createReservationPreorder({
        restaurant_id,
        date,
        number_of_people,
        duration,
        items,
        card_id
      })
        .then(data => {
          this.props.navigator.pop()
          ToastAndroid.show('Reservation made successfully', ToastAndroid.LONG)
        })
        .catch(err => {
          console.log(err)
          this.props.navigator.showLocalAlert(err.graphQLErrors[0].message, Alerts.error)
        })
        .then(() => {
          this.setState({
            isRequestInFlight: false
          })
        })
    } else {
      this.props.createReservation({
        restaurant_id,
        date,
        number_of_people,
        duration
      })
        .then(data => {
          this.props.navigator.pop()
          ToastAndroid.show('Reservation made successfully', ToastAndroid.LONG)
        })
        .catch(err => {
          this.props.navigator.showLocalAlert(err.graphQLErrors[0].message, Alerts.error)
        })
        .then(() => {
          this.setState({
            isRequestInFlight: false
          })
        })
    }
  }

  _renderMakeReservation () {
    let { isRequestInFlight } = this.state
    let loadingIndicator
    if (isRequestInFlight) {
      loadingIndicator = (
        <View style={styles.loadingIndicatorContainer}>
          <ActivityIndicator size='small' color={'#fff'} />
        </View>
      )
    }
    let reservationDate = new Date(this.state.date)
    reservationDate.setHours(this.state.time.hour)
    reservationDate.setMinutes(this.state.time.minute)

    return (
      <View>
        <TouchableNativeFeedbackSafe
          background={TouchableNativeFeedbackSafe.Ripple()}
          delayPressIn={0}
          onPress={() => this._handleMake(this.props.restaurant.id, reservationDate, this.state.number_of_people,
            this.state.duration, this.state.items, this.state.selectedCardId)}
          style={styles.payButton}>
          <RegularText style={styles.enterText}>Make</RegularText>
        </TouchableNativeFeedbackSafe>
        {loadingIndicator}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  payButton: {
    paddingVertical: 10,
    borderRadius: 2,
    backgroundColor: Colors.tintColor,
    alignItems: 'center',
    justifyContent: 'center'
  },
  itemImage: {
    width: 256 / 4.0,
    height: 256 / 4.0
  },
  itemContainer: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 8,
    flexDirection: 'row',
    height: 75,
    marginHorizontal: 2,
    borderRadius: 5
  },
  totalContainer: {
    marginTop: 10,
    borderTopColor: '#000',
    borderTopWidth: 1,
    alignItems: 'flex-end'
  },
  priceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  itemDescription: {
    flexDirection: 'column',
    flex: 1,
    marginLeft: 10
  },
  container: {
    flex: 1
  },
  prop: {
    flexDirection:'row'
  },
  loadingIndicatorContainer: {
    position: 'absolute',
    right: 20,
    paddingTop: 2,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  contentContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15
  },
  enterText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: '#f7f5f5'
  }
})
