/**
 * @providesModule ReservationsScreen
 * @flow
 */
import React from 'react'
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
  RefreshControl,
  View
} from 'react-native'
import TouchableNativeFeedbackSafe from '@exponent/react-native-touchable-native-feedback-safe'
import { capitalize } from 'lodash'
import { connect } from 'react-redux'
import Actions from 'Actions'
import Alerts from 'Alerts'
import Colors from 'Colors'
import Router from 'Router'
import { MaterialIcons } from '@exponent/vector-icons'
import { RegularText } from 'StyledText'
import { SlidingTabNavigation, SlidingTabNavigationItem, withNavigation } from '@exponent/ex-navigation'
import ReservationsListView from 'ReservationsListView'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'

const query = gql`
    query {
        user {
            reservations {
                id
                table {
                    id
                    hall {
                        name
                        restaurant {
                            id
                            name
                        }
                        tables {
                            id
                            scheme
                            number_of_people
                        }
                    }
                }
                date
                number_of_people
                preorder {
                    id
                    payed
                    items {
                        item {
                            id
                            name
                            price
                            time_to_prepare
                            ingredients
                            liquid
                            weight
                        }
                        quantity
                        state
                    }
                }
            }
        }
    }
`

@compose(
  graphql(query, {
    name: 'reservationsData'
  })
)
export default class ReservationsScreen extends React.Component {
  static route = {
    navigationBar: {
      title: 'Reservations',
      ...SlidingTabNavigation.navigationBarStyles
    }
  }

  state = {
    refreshing: false
  }

  shouldComponentUpdate () {
    if (this.props.reservationsData) {
      this.props.reservationsData.refetch()
    }

    return true
  }

  render () {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
          automaticallyAdjustContentInsets={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this._onRefresh()}
              tintColor={Colors.veryLightGrey}
              colors={[Colors.tintColor]}
            />
          }>
          <ReservationsListView
            onCancel={() => this._onCancelReservation()}
            loading={this.props.reservationsData.loading}
            reservations={this.props.reservationsData.user ? this.props.reservationsData.user.reservations : []} />
        </ScrollView>
      </View>
    )
  }

  _onRefresh () {
    this.setState({
      refreshing: true
    })
    this.props.reservationsData.refetch().then(() => {
      this.setState({
        refreshing: false
      })
    })
  }

  _onCancelReservation () {
    this.props.reservationsData.refetch()
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 80
  },
  cancelReservationButtonText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: Colors.tintColor
  },
  cancelReservationButton: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 2,
    backgroundColor: '#f7f5f5',
    alignItems: 'center',
    justifyContent: 'center'
  },
  footer: {
    position: 'absolute',
    flex: 1,
    left: 0,
    right: 0,
    bottom: -10,
    paddingHorizontal: 10,
    backgroundColor: '#fff',
    flexDirection: 'row',
    height: 80,
    alignItems: 'center'
  },
  contentContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15
  },
  tabIndicator: {
    backgroundColor: '#FFEB3B'
  }
})
