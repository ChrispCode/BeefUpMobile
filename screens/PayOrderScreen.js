/**
 * @providesModule PayOrderScreen
 * @flow
 */
import React from 'react'
import {
  Image,
  ScrollView,
  StyleSheet,
  View,
  ListView,
  Picker,
  TouchableOpacity,
  ActivityIndicator,
  ToastAndroid,
  RefreshControl
} from 'react-native'
import Colors from 'Colors'
import Alerts from 'Alerts'
import { connect } from 'react-redux'
import validPrice from '../utilities/validPrice'
import TouchableNativeFeedbackSafe from '@exponent/react-native-touchable-native-feedback-safe'
import { BoldText, RegularText } from 'StyledText'
import { graphql, compose } from 'react-apollo'
import { withNavigation } from '@exponent/ex-navigation'
import { MaterialIcons } from '@exponent/vector-icons'
import CardsListView from 'CardsListView'
import Router from 'Router'
import gql from 'graphql-tag'
const Item = Picker.Item

@connect(data => EditReservation.getDataProps)
@withNavigation
class EditReservation extends React.Component {
  static getDataProps (data) {
    return {
      currentUser: data.currentUser
    }
  }

  render () {
    if (this.props.order.reservation) {
      return (
        <TouchableOpacity
          style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginRight: 15}}
          onPress={() => this.props.navigator.push(Router.getRoute('reservation', {
            reservation: this.props.order.reservation
          }))}>
          <MaterialIcons name='event' size={28} color='#000'/>
        </TouchableOpacity>
      )
    } else {
      return (
        <TouchableOpacity
          style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginRight: 15}}
          onPress={() => this.props.navigator.push(Router.getRoute('editOrder', {
            onEditOrder: this.props.onEditOrder,
            order: this.props.order,
            restaurant_id: this.props.currentUser.restaurant_id
          }))}>
          <MaterialIcons name='mode-edit' size={28} color='#000'/>
        </TouchableOpacity>
      )
    }
  }
}

const mutation = gql`
    mutation($id: Int!, $payment_method: PaymentMethods, $card_id: Int) {
        payOrder(id: $id, payment_method: $payment_method, card_id: $card_id) {
            payed
        }
    }
`

// @connect(data => RestaurantScreen.getDataProps)
@connect(data => PayOrderScreen.getDataProps)
@compose(
  graphql(mutation, {
    props: ({ ownProps, mutate }) => ({
      payOrder: ({ id, payment_method, card_id }) => {
        return new Promise(resolve => {
          return resolve(mutate({
            variables: {
              id,
              payment_method,
              card_id
            }
          }))
        })
      }
    })
  })
)
export default class PayOrderScreen extends React.Component {
  constructor (props, context) {
    super(props, context)

    let dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2
    })

    if (props.items) {
      dataSource = dataSource.cloneWithRows(props.items)
    }

    this.state = {
      dataSource,
      payment_method: 'cash',
      selectedCardId: null,
      isRequestInFlight: false
    }
  }

  static getDataProps (data) {
    return {
      currentUser: data.currentUser
    }
  }

  static route = {
    navigationBar: {
      title ({ order, prepaid }) {
        if (order.reservation || prepaid) {
          return 'Order - prepaid'
        }
        return 'Order'
      },
      renderRight ({ params }) {
        if (params.prepaid) {
          return null
        }

        return (<EditReservation order={params.order} onEditOrder={params.onEditOrder} />)
      }
    }
  };

  render () {
    let total = 0
    this.props.items.map(item => {
      total += (item.item.price * item.quantity)
    })

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        automaticallyAdjustContentInsets={false}>
        <ListView
          style={{ flex: 1 }}
          contentContainerStyle={{ backgroundColor: '#fff' }}
          dataSource={this.state.dataSource}
          renderRow={this._renderItem} />
        <View style={styles.totalContainer}>
          <RegularText>Total: <BoldText>{validPrice(total)}</BoldText></RegularText>
        </View>
        {(this.props.currentUser.role === 'normal' && !this.props.order.reservation && !this.props.prepaid) &&
        <View style={{ marginTop: 10 }}>
          <Picker
            style={styles.picker}
            selectedValue={this.state.payment_method}
            onValueChange={value => this.setState({ payment_method: value })}
            mode='dropdown'>
            <Item label='In cash' value='cash' />
            <Item label='By card' value='card' />
          </Picker>
          {this.state.payment_method === 'card' && this._renderCards()}
          {this._renderPayButton()}
        </View>
        }
      </ScrollView>
    )
  }

  _renderCards () {
    return (
      <CardsListView
        cards={this.props.cards}
        onSelectCard={(card) => this._handleSelectCard(card)}
        selectedCardId={this.state.selectedCardId} />
    )
  }

  _handleSelectCard (card) {
    this.setState({
      selectedCardId: card.id
    })
  }

  _handlePay (id, payment_method) {
    this.setState({
      isRequestInFlight: true
    })
    let cardId = this.state.payment_method === 'card' ? this.state.selectedCardId : null

    this.props.payOrder({
      id,
      payment_method,
      card_id: cardId
    })
      .then(data => {
        this.props.onEditOrder()
        this.props.navigator.pop()
        ToastAndroid.show('Order payment successful', ToastAndroid.LONG)
      })
      .catch(err => {
        console.log(err)
        this.props.navigator.showLocalAlert(err.graphQLErrors[0].message, Alerts.error)
      })
      .then(() => {
        this.setState({
          isRequestInFlight: false
        })
      })
  }

  _renderPayButton () {
    let { isRequestInFlight } = this.state
    let loadingIndicator
    if (isRequestInFlight) {
      loadingIndicator = (
        <View style={styles.loadingIndicatorContainer}>
          <ActivityIndicator size='small' color={'#fff'} />
        </View>
      )
    }

    return (
      <View>
        <TouchableNativeFeedbackSafe
          background={TouchableNativeFeedbackSafe.Ripple()}
          delayPressIn={0}
          onPress={() => this._handlePay(this.props.order.id, this.state.payment_method)}
          style={styles.payButton}>
          <RegularText style={styles.enterText}>Pay</RegularText>
        </TouchableNativeFeedbackSafe>
        {loadingIndicator}
      </View>
    )
  }

  _renderItem = (item) => {
    let total = item.item.price * item.quantity;
    return (
      <View style={{
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row'
      }}>
        <View style={styles.itemContainer}>
          <Image
            style={styles.itemImage}
            source={require('../assets/images/meal.png')}
          />
          <View style={styles.itemDescription}>
            <View style={styles.priceContainer}>
              <BoldText>{item.item.name}</BoldText>
              <RegularText>{validPrice(item.item.price)} * {item.quantity} =
                <BoldText>{validPrice(total)}</BoldText></RegularText>
            </View>
            {this._renderIngredients(item.item.ingredients)}
          </View>
        </View>
      </View>
    )
  }

  _renderIngredients (ingredients) {
    let toShow = ''
    if (ingredients) {
      ingredients.map(ingredient => {
        toShow += ingredient + ', '
      })
      toShow = toShow.substring(0, toShow.length - 2)
    }
    return (
      <RegularText>{toShow}</RegularText>
    )
  }
}

const styles = StyleSheet.create({
  payButton: {
    paddingVertical: 10,
    borderRadius: 2,
    backgroundColor: Colors.tintColor,
    alignItems: 'center',
    justifyContent: 'center'
  },
  itemImage: {
    width: 256 / 4.0,
    height: 256 / 4.0
  },
  itemContainer: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 8,
    flexDirection: 'row',
    height: 75,
    marginHorizontal: 2,
    borderRadius: 5
  },
  totalContainer: {
    marginTop: 10,
    borderTopColor: '#000',
    borderTopWidth: 1,
    alignItems: 'flex-end'
  },
  priceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  itemDescription: {
    flexDirection: 'column',
    flex: 1,
    marginLeft: 10
  },
  container: {
    flex: 1
  },
  loadingIndicatorContainer: {
    position: 'absolute',
    right: 20,
    paddingTop: 2,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  contentContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15
  },
  enterText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: '#f7f5f5'
  }
})
