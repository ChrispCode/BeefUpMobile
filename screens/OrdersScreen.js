/**
 * @providesModule OrdersScreen
 * @flow
 */
import React from 'react'
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
  ToastAndroid,
  View
} from 'react-native'
import TouchableNativeFeedbackSafe from '@exponent/react-native-touchable-native-feedback-safe'
import { capitalize } from 'lodash'
import { connect } from 'react-redux'
import Actions from 'Actions'
import Alerts from 'Alerts'
import Colors from 'Colors'
import Router from 'Router'
import { MaterialIcons } from '@exponent/vector-icons'
import { RegularText } from 'StyledText'
import { SlidingTabNavigation, SlidingTabNavigationItem, withNavigation } from '@exponent/ex-navigation'
import ScannerView from 'ScannerView'
import RestaurantOrdersView from 'RestaurantOrdersView'
import OrdersListView from 'OrdersListView'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'

const mutation = gql`
    mutation {
        quitRestaurant
    }
`

@connect(data => QuitButton.getDataProps)
@compose(
  graphql(mutation, {
    props: ({ mutate }) => ({
      quitRestaurant: () => new Promise(resolve => {
        return resolve(mutate())
      })
    })
  })
)
@withNavigation
class QuitButton extends React.Component {
  static getDataProps (data) {
    return {
      currentUser: data.currentUser
    }
  }

  _handleQuitRestaurant () {
    this.props.dispatch(Actions.showGlobalLoading())
    this.props.quitRestaurant()
      .then(() => {
        this.props.dispatch(Actions.quitRestaurant())
        ToastAndroid.show('You are no longer authorized', ToastAndroid.LONG)
      })
      .catch(err => {
        this.props.navigator.showLocalAlert(err.graphQLErrors[0].message, Alerts.error)
      })
      .then(() => {
        this.props.dispatch(Actions.hideGlobalLoading())
      })
  }

  render () {
    if (this.props.currentUser && this.props.currentUser.role === 'normal' && this.props.currentUser.restaurant_id)
      return (
        <TouchableOpacity
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginRight: 15 }}
          onPress={() => this._handleQuitRestaurant()}>
          <MaterialIcons name='exit-to-app' size={28} color='#fff' />
        </TouchableOpacity>
      )

    return null
  }
}

const query = gql`
    query {
        user {
            orders {
                id
                payed
                items {
                    item {
                        id
                        name
                        price
                        time_to_prepare
                        ingredients
                        liquid
                        weight
                    }
                    quantity
                    state
                }
                reservation {
                    id
                    table {
                        id
                        hall {
                            name
                            restaurant {
                                id
                                name
                            }
                            tables {
                                id
                                scheme
                            }
                        }
                    }
                    date
                    number_of_people
                }
            }
            cards {
                id
                card_id
                last4
                scheme
            }
        }
    }
`

@connect(data => OrdersScreen.getDataProps)
@compose(
  graphql(query, {
    name: 'ordersData',
    options: ({ currentUser }) => ({
      skip: currentUser.role !== 'normal'
    })
  })
)
export default class OrdersScreen extends React.Component {
  static route = {
    navigationBar: {
      title: 'Orders',
      ...SlidingTabNavigation.navigationBarStyles,
      renderRight () {
        return <QuitButton />
      }
    }
  };

  state = {
    channel: null,
    refreshing: false
  }

  static getDataProps (data) {
    return {
      currentUser: data.currentUser,
      pusher: data.pusher
    }
  }

  // shouldComponentUpdate () {
  //   console.log(this.props.currentUser)
  //   if (this.props.ordersData && this.props.ordersData.user && this.props.currentUser.token)
  //     this.props.ordersData.refetch()
  //
  //   return true
  // }

  componentWillMount () {
    if (this.props.ordersData && this.props.ordersData.user)
      this.props.ordersData.refetch()
  }

  render() {
    if (this.props.currentUser.role === 'normal') {
      return (
        <View style={{ flex: 1 }}>
          <ScrollView
            style={styles.container}
            contentContainerStyle={styles.contentContainer}
            automaticallyAdjustContentInsets={false}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={() => this._handleRefresh()}
                tintColor={Colors.veryLightGrey}
                colors={[Colors.tintColor]}
              />
            }>
            <OrdersListView
              onEditOrder={() => this._onEditOrder()}
              loading={this.props.ordersData.loading}
              cards={this.props.ordersData.user ? this.props.ordersData.user.cards : []}
              orders={this.props.ordersData.user ? this.props.ordersData.user.orders : []} />
          </ScrollView>
          {this.props.currentUser.restaurant_id &&
          <View style={styles.footer}>
            <TouchableNativeFeedbackSafe
              background={TouchableNativeFeedbackSafe.Ripple()}
              delayPressIn={0}
              onPress={() => {
                this._handleCreateOrder(this.props.currentUser.restaurant_id)
              }}
              style={styles.cancelReservationButton}>
              <RegularText style={styles.cancelReservationButtonText}>Order</RegularText>
            </TouchableNativeFeedbackSafe>
          </View>
          }
        </View>
      )
    }

    return (
      <View style={{ flex: 1 }}>
        <SlidingTabNavigation
          initialTab='orders'
          barBackgroundColor={Colors.tintColor}
          position='top'
          renderLabel={this._renderLabel}
          pressColor='rgba(0,0,0,0.2)'
          indicatorStyle={styles.tabIndicator}>
          <SlidingTabNavigationItem id='orders'>
            {this._renderRestaurantOrders()}
          </SlidingTabNavigationItem>

          <SlidingTabNavigationItem id='scanner'>
            {this._renderScanner()}
          </SlidingTabNavigationItem>

        </SlidingTabNavigation>
      </View>
    )
  }

  _handleRefresh () {
    this.setState({
      refreshing: true
    })
    this.props.ordersData.refetch()
      .then(() => {
        this.setState({
          refreshing: false
        })
      })
  }

  _onEditOrder () {
    this.props.ordersData.refetch()
  }

  _renderRestaurantOrders () {
    return <RestaurantOrdersView />
  }

  _handleCreateOrder (restaurant_id) {
    this.props.navigator.push(Router.getRoute('order', {
      onCreateOrder: () => this._onEditOrder(),
      restaurant_id
    }))
  }

  _renderScanner () {
    return (
      <ScannerView />
    )
  }

  _renderLabel({ route }) {
    let title = capitalize(route.key)

    return (
      <RegularText style={{ color: Colors.navigationBarTintColor }}>
        {title}
      </RegularText>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 80
  },
  cancelReservationButtonText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: Colors.tintColor
  },
  cancelReservationButton: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 2,
    backgroundColor: '#f7f5f5',
    alignItems: 'center',
    justifyContent: 'center'
  },
  footer: {
    position: 'absolute',
    flex: 1,
    left: 0,
    right: 0,
    bottom: -10,
    paddingHorizontal: 10,
    backgroundColor: '#fff',
    flexDirection: 'row',
    height: 80,
    alignItems: 'center'
  },
  contentContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15
  },
  tabIndicator: {
    backgroundColor: '#FFEB3B'
  }
})
