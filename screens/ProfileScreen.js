/**
 * @providesModule ProfileScreen
 * @flow
 */
import React from 'react'
import {
  ScrollView,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  RefreshControl,
  ToastAndroid,
  KeyboardAvoidingView,
  Modal,
  WebView,
  View
} from 'react-native'
import { connect } from 'react-redux'
import TouchableNativeFeedbackSafe from '@exponent/react-native-touchable-native-feedback-safe'
import { capitalize } from 'lodash'
import Actions from 'Actions'
import Alerts from 'Alerts'
import Colors from 'Colors'
import StyledTextInput from 'StyledTextInput'
import { LightText, RegularText, BoldText } from 'StyledText'
import { SlidingTabNavigation, withNavigation } from '@exponent/ex-navigation'
import createInvoke from 'react-native-webview-invoke/native'
import { graphql, compose } from 'react-apollo'
import CardsListView from 'CardsListView'
import gql from 'graphql-tag'

const query = gql`
    query {
        user {
            username
            email
            first_name
            last_name
            cards {
                last4
                scheme
            }
        }
    }
`

@compose(
  graphql(query, {
    name: 'userData'
  })
)
export default class ProfileScreen extends React.Component {
  static route = {
    navigationBar: {
      title: 'Profile',
      ...SlidingTabNavigation.navigationBarStyles
    }
  }

  state = {
    loadingCards: false,
    refreshing: false
  }

  render () {
    return (
      <Account
        onAddCard={() => this._handleAddCard()}
        loadingCards={this.state.loadingCards}
        loading={this.props.userData.loading}
        onRefresh={() => this._handleRefresh()}
        refreshing={this.state.refreshing}
        user={this.props.userData.user} />
    )
  }

  _handleRefresh () {
    this.setState({
      refreshing: true
    })
    this.props.userData.refetch()
      .then(() => {
        this.setState({
          refreshing: false
        })
      })
  }

  _handleAddCard () {
    this.setState({
      loadingCards: true
    })
    this.props.userData.refetch()
      .then(() => {
        this.setState({
          loadingCards: false
        })
      })
  }
}

const mutationUser = gql`
    mutation ($password_confirmation: String!, $first_name: String, $last_name: String,
        $email: String, $password: String ) {
        updateUser(first_name: $first_name, last_name: $last_name, email: $email,
            password: $password, password_confirmation: $password_confirmation) {
            id
        }
    }
`

const mutationCard = gql`
    mutation ($card_id: String!) {
        addCard(card_id: $card_id) {
            id
        }
    }
`

@connect()
@compose(graphql(mutationUser, {
  props: ({ mutate }) => ({
    updateUser: ({ first_name, last_name, email, password, password_confirmation }) => {
      return new Promise(resolve => {
        return resolve(mutate({
          variables: {
            first_name,
            last_name,
            email,
            password,
            password_confirmation
          }
        }))
      })
    }
  })
}), graphql(mutationCard, {
  props: ({ mutate }) => ({
    addCard: ({ card_id }) => {
      return new Promise(resolve => {
        return resolve(mutate({
          variables: {
            card_id
          }
        }))
      })
    }
  })
}))
@withNavigation
class Account extends React.Component {
  state = {
    isRequestInFlight: false,
    isRequestCardInFlight: false,
    requestedToAddCard: false,
    user: {}
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      user: nextProps.user
    })
  }

  render () {
    if (this.props.loading) {
      return this._renderLoading()
    } else {
      return (
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
          automaticallyAdjustContentInsets={false}
          refreshControl={
            <RefreshControl
              refreshing={this.props.refreshing}
              onRefresh={() => this.props.onRefresh()}
              tintColor={Colors.veryLightGrey}
              colors={[Colors.tintColor]}
            />
          }>
          <View style={styles.category}>
            <KeyboardAvoidingView behavior='padding'>
              <View style={styles.prop}>
                <LightText style={styles.categoryTitle}>Account</LightText>
                <LightText style={styles.categorySubtitle}> - {this.props.user.username}</LightText>
              </View>
              {this._renderInputGroup('First name', 'first_name')}
              {this._renderInputGroup('Last name', 'last_name')}
              {this._renderInputGroup('Email', 'email')}
              {this._renderInputGroup('New password', 'new_password', true)}
              {this._renderInputGroup('Confirmation password', 'password_confirmation', true)}
              {this._renderEditButton()}
            </KeyboardAvoidingView>
          </View>
          <View style={styles.category}>
            <View style={styles.prop}>
              <LightText style={styles.categoryTitle}>Cards</LightText>
            </View>
            <CardsListView loading={this.props.loading} cards={this.props.user.cards} />
            {this._renderAddButton()}
          </View>
          {this._renderCardModal()}
        </ScrollView>
      )
    }
  }

  _handleEditUser = () => {
    let { isRequestInFlight, user } = this.state

    if (isRequestInFlight) {
      return
    }

    if (!user.password_confirmation) {
      this.props.navigator.showLocalAlert('Please enter your confirmation password', Alerts.error)
      return
    }

    this.setState({ isRequestInFlight: true })

    this.props.updateUser({
      password_confirmation: user.password_confirmation,
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
      password: user.password
    })
      .then(data => {
        ToastAndroid.show('Successfully updated your account', ToastAndroid.LONG)
        let user = Object.assign({}, this.state.user)
        user.password_confirmation = ''
        this.setState({
          user,
          isRequestInFlight: false
        })
      })
      .catch(err => {
        this.props.navigator.showLocalAlert(err.graphQLErrors[0].message, Alerts.error)
        this.setState({ isRequestInFlight: false })
      })
  }

  _renderEditButton () {
    let { isRequestInFlight } = this.state
    let loadingIndicator
    if (isRequestInFlight) {
      loadingIndicator = (
        <View style={styles.loadingIndicatorContainer}>
          <ActivityIndicator size='small' color={Colors.midGrey} />
        </View>
      )
    }

    return (
      <View style={{ marginTop: 10 }}>
        <TouchableNativeFeedbackSafe
          background={TouchableNativeFeedbackSafe.Ripple()}
          delayPressIn={0}
          onPress={this._handleEditUser}
          style={styles.doneButton}>
          <RegularText style={styles.doneText}>Edit</RegularText>
        </TouchableNativeFeedbackSafe>
        {loadingIndicator}
      </View>
    )
  }

  _renderAddButton () {
    let { isRequestCardInFlight } = this.state
    let loadingIndicator
    if (isRequestCardInFlight || this.props.loadingCards) {
      loadingIndicator = (
        <View style={styles.loadingIndicatorContainer}>
          <ActivityIndicator size='small' color={Colors.midGrey} />
        </View>
      )
    }

    return (
      <View style={{ marginTop: 10 }}>
        <TouchableNativeFeedbackSafe
          background={TouchableNativeFeedbackSafe.Ripple()}
          delayPressIn={0}
          onPress={this._handleCardModal}
          style={styles.doneButton}>
          <RegularText style={styles.doneText}>Add</RegularText>
        </TouchableNativeFeedbackSafe>
        {loadingIndicator}
      </View>
    )
  }

  _handleCardModal = () => {
    if (this.state.isRequestCardInFlight) {
      return
    }

    if (!this.state.requestedToAddCard) {
      this.props.dispatch(Actions.showGlobalLoading())
    }

    this.setState({
      requestedToAddCard: !this.state.requestedToAddCard
    })
  }

  _renderInputGroup (label, field, secure) {
    return (
      <View>
        <RegularText style={styles.title}>{label}</RegularText>
        <StyledTextInput
          autoCorrect={false}
          autoCapitalize='none'
          blurOnSubmit={false}
          onChangeText={value => {
            let user = Object.assign({}, this.state.user)
            user[field] = value
            this.setState({ user })
          }}
          value={this.state.user[field]}
          keyboardType='default'
          placeholder={label}
          secureTextEntry={!!secure}
          style={styles.propInput}
        />
      </View>
    )
  }

  _renderLoading () {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator color={Colors.midGrey}/>
      </View>
    )
  }

  webview: WebView
  invoke = createInvoke(() => this.webview)
  webInitialize = () => {
    this.props.dispatch(Actions.hideGlobalLoading())
  }
  webWannaSet = (data) => {
    switch (data) {
      case 'closed': {
        this._handleCardModal()
        return
      }
      default: {
        this._handleAddCard(data)
        this._handleCardModal()
        return
      }
    }
  }

  componentDidMount () {
    this.invoke
      .define('init', this.webInitialize)
      .define('get', this.webWannaGet)
      .define('set', this.webWannaSet)
  }

  _handleAddCard ({ card }) {
    this.setState({
      isRequestCardInFlight: true
    })
    this.props.addCard({
      card_id: card.id
    })
      .then(data => {
        this.props.onAddCard()
        ToastAndroid.show('Successfully added the new card', ToastAndroid.LONG)
      })
      .catch(err => {
        console.log(err)
        this.props.navigator.showLocalAlert(err.graphQLErrors[0].message, Alerts.error)
      })
      .then(() => {
        this.setState({
          isRequestCardInFlight: false
        })
      })
  }

  _renderCardModal () {
    return (
      <Modal
        animationType={'slide'}
        transparent={false}
        visible={this.state.requestedToAddCard}
        onRequestClose={this._handleCardModal}>
        <WebView
          ref={webview => this.webview = webview}
          onMessage={this.invoke.listener}
          source={{uri: 'http://services.beefup.eu/paylike/card/webview'}}
        />
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  cancelReservationButtonText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: Colors.tintColor
  },
  title: {
    fontSize: 16,
    color: Colors.darkGrey,
    marginTop: 10
  },
  cancelReservationButton: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 2,
    backgroundColor: '#f7f5f5',
    alignItems: 'center',
    justifyContent: 'center'
  },
  category: {
    marginBottom: 15
  },
  categoryTitle: {
    fontSize: 28,
    marginBottom: 15
  },
  categorySubtitle: {
    fontSize: 18,
    marginTop: 10
  },
  doneButton: {
    paddingVertical: 10,
    borderRadius: 2,
    backgroundColor: '#f7f5f5',
    alignItems: 'center',
    justifyContent: 'center'
  },
  doneText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: Colors.tintColor
  },
  footer: {
    position: 'absolute',
    flex: 1,
    left: 0,
    right: 0,
    bottom: -10,
    paddingHorizontal: 10,
    backgroundColor: '#fff',
    flexDirection: 'row',
    height: 80,
    alignItems: 'center'
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingBottom: 80,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingIndicatorContainer: {
    position: 'absolute',
    right: 20,
    paddingTop: 2,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  contentContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15
  },
  prop: {
    flexDirection:'row'
  },
  propInput: {
    color: '#000',
    marginBottom: 0
  },
  tabIndicator: {
    backgroundColor: '#FFEB3B'
  }
})
