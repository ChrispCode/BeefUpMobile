/**
 * @providesModule ReservationScreen
 * @flow
 */
import React from 'react'
import { ScrollView, TouchableOpacity, StyleSheet, ToastAndroid,
  View, ActivityIndicator, TouchableHighlight, Modal } from 'react-native'
import { connect } from 'react-redux'
import Colors from 'Colors'
import Alerts from 'Alerts'
import QRCode from 'react-native-qrcode'
import { BoldText, RegularText, LightText } from 'StyledText'
import Actions from 'Actions'
import validDate from '../utilities/validDate'
import TouchableNativeFeedbackSafe from '@exponent/react-native-touchable-native-feedback-safe'
import { graphql, compose } from 'react-apollo'
import { withNavigation } from '@exponent/ex-navigation'
import gql from 'graphql-tag'
import Router from 'Router'
import Floorplan from 'Floorplan'

const mutation = gql`
    mutation ($id: Int!) {
        removeReservation(id: $id) {
            id
        }
    }
`

@connect(data => ReservationScreen.getDataProps)
@compose(
  graphql(mutation, {
    props: ({ ownProps, mutate }) => ({
      removeReservation: ({ id }) => {
        return new Promise(resolve => {
          return resolve(mutate({
            variables: {
              id
            }
          }))
        })
      }
    })
  })
)
@withNavigation
export default class ReservationScreen extends React.Component {
  constructor (props, context) {
    super(props, context)

    this.state = {
      restaurant: this.props.reservation.table.hall.restaurant,
      reservation: this.props.reservation,
      requestedToEnter: false,
      requestedToTable: false
    }
  }

  static getDataProps (data) {
    return {
      currentUser: data.currentUser,
      pusher: data.pusher
    }
  }

  static route = {
    navigationBar: {
      title ({ reservation }) {
        return reservation.table.hall.restaurant.name
      }
    }
  };

  render () {
    if (this.state.loading) {
      return (
        <View style={styles.loadingContainer}>
          <ActivityIndicator color={Colors.midGrey} />
          <LightText>Loading...</LightText>
        </View>
      )
    }

    return (
      <View style={{
        flex: 1
      }}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
          automaticallyAdjustContentInsets={false}>
          <BoldText style={styles.title}>Date</BoldText>
          <RegularText>{validDate(this.state.reservation.date)}</RegularText>
          <BoldText style={styles.title}>People - {this.state.reservation.number_of_people}</BoldText>
          <TouchableHighlight
            underlayColor={Colors.veryLightGrey}
            onPress={() => this._selectRestaurant(this.state.restaurant)}>
            <View>
            <BoldText style={styles.title}>Restaurant</BoldText>
            <RegularText>{this.state.restaurant.name}</RegularText>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            underlayColor={Colors.veryLightGrey}
            onPress={() => this._handleTable(this.state.reservation.table)}>
            <View>
              <BoldText style={styles.title}>Table</BoldText>
            </View>
          </TouchableHighlight>
          {this.state.reservation.preorder &&
          <TouchableHighlight
            underlayColor={Colors.veryLightGrey}
            onPress={() => this.props.navigator.push(Router.getRoute('payOrder', {
              order: this.state.reservation.preorder,
              items: this.state.reservation.preorder.items,
              prepaid: true
            }))}>
            <View>
              <BoldText style={styles.title}>Order - prepaid</BoldText>
            </View>
          </TouchableHighlight>
          }
        </ScrollView>
        <View style={styles.footer}>
          {(this.props.onCancel && !this.state.reservation.preorder) &&
            <TouchableNativeFeedbackSafe
              background={TouchableNativeFeedbackSafe.Ripple()}
              delayPressIn={0}
              onPress={() => {
                this._handleCancelReservation()
              }}
              style={[styles.cancelReservationButton, {marginRight: 10}]}>
              <RegularText style={styles.cancelReservationButtonText}>Cancel</RegularText>
            </TouchableNativeFeedbackSafe>
          }
          <TouchableNativeFeedbackSafe
            background={TouchableNativeFeedbackSafe.Ripple()}
            delayPressIn={0}
            onPress={() => {
              this._handleEnter()
            }}
            style={[ styles.enterButton, {marginLeft: (this.props.onCancel || !this.state.reservation.preorder) ? 0 : 10} ]}>
            <RegularText style={styles.enterText}>Enter</RegularText>
          </TouchableNativeFeedbackSafe>
        </View>

        {this._renderModal()}
        <Floorplan
          visible={this.state.requestedToTable}
          hall={this.props.reservation.table.hall}
          tables={this.props.reservation.table.hall.tables}
          selectedId={this.props.reservation.table.id}
          onRequestClose={() => this._handleTable()}
        />
      </View>
    )
  }

  _handleEnter = () => {
    this.setState({
      requestedToEnter: !this.state.requestedToEnter
    })
  }

  _handleTable = () => {
    this.setState({
      requestedToTable: !this.state.requestedToTable
    })
  }

  _renderModal () {
    let barcode = JSON.stringify({
      restaurant_id: this.props.reservation.table.hall.restaurant.id,
      client_id: this.props.currentUser.id,
      reservation_id: this.props.reservation.id
    })
    return (
      <Modal
        animationType={'slide'}
        transparent={false}
        visible={this.state.requestedToEnter}
        onRequestClose={this._handleEnter}>
        <View style={styles.enterModal}>
          <View />
          <QRCode
            value={barcode}
            size={200}
            bgColor={Colors.tintColor}
            fgColor='#fff' />
          <View style={{ marginTop: 10 }}>
            <RegularText style={styles.closeText} onPress={this._handleEnter}>Cancel</RegularText>
          </View>
        </View>
      </Modal>
    )
  }

  _selectRestaurant (restaurant) {
    this.props.navigation.getNavigator('root').push(Router.getRoute('restaurant', { restaurant }))
  }

  _handleCancelReservation () {
    this.props.dispatch(Actions.showGlobalLoading())
    this.props.removeReservation({
      id: this.props.reservation.id
    })
      .then(data => {
        this.props.onCancel()
        this.props.navigator.popToTop()
        ToastAndroid.show('You successfully canceled your reservation', ToastAndroid.LONG)
      })
      .catch(err => {
        this.props.navigator.showLocalAlert(err.graphQLErrors[0].message, Alerts.error)
      })
      .then(() => {
        this.props.dispatch(Actions.hideGlobalLoading())
      })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  loadingContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelReservationButtonText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: Colors.tintColor
  },
  cancelReservationButton: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 2,
    backgroundColor: '#f7f5f5',
    alignItems: 'center',
    justifyContent: 'center'
  },
  enterButton: {
    flex: 1,
    paddingVertical: 10,
    borderRadius: 2,
    backgroundColor: Colors.tintColor,
    alignItems: 'center',
    justifyContent: 'center'
  },
  enterText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: '#f7f5f5'
  },
  title: {
    fontSize: 16,
    color: Colors.darkGrey,
    marginTop: 10,
    marginBottom: 10
  },
  enterModal: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 50
  },
  footer: {
    position: 'absolute',
    flex: 1,
    left: 0,
    right: 0,
    bottom: -10,
    paddingHorizontal: 10,
    backgroundColor: '#fff',
    flexDirection: 'row',
    height: 80,
    alignItems: 'center'
  },
  contentContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15
  }
})
