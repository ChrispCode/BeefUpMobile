/**
 * @providesModule RestaurantsScreen
 * @flow
 */
import React from 'react'
import { Platform, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import Actions from 'Actions'
import Colors from 'Colors'
import { RegularText } from 'StyledText'
import StyledTextInput from 'StyledTextInput'
import { SlidingTabNavigation } from '@exponent/ex-navigation'
import RestaurantsListView from 'RestaurantsListView'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'

const query = gql`
    query {
        restaurants {
            id
            name
            city
            country
        }
    }
`

class SettingsButton extends React.Component {
  render () {
    return (
      <TouchableOpacity
        style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginRight: 15 }}
        onPress={() => this.props.dispatch(Actions.signOut())}>
        <RegularText style={{ color: '#fff' }}>Sign Out</RegularText>
      </TouchableOpacity>
    )
  }
}

@connect()
@compose(
  graphql(query, { name: 'restaurantsData' })
)
export default class RestaurantsScreen extends React.Component {
  static route = {
    navigationBar: {
      title: 'Restaurants',
      ...SlidingTabNavigation.navigationBarStyles,
      renderRight () {
        if (Platform.OS === 'ios') {
          return <SettingsButton />
        }
      }
    }
  };
  state = {
    restaurantsFilter: '',
    restaurants: [],
    filteredRestaurants: []
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      restaurants: nextProps.restaurantsData.restaurants,
      filteredRestaurants: nextProps.restaurantsData.restaurants,
      restaurantsFilter: ''
    })
  }

  render () {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
          automaticallyAdjustContentInsets={false}>
          <StyledTextInput
            autoCorrect={false}
            autoCapitalize='none'
            blurOnSubmit={false}
            onChangeText={value => {
              this.setState({restaurantsFilter: value})
              this._handleFilterRestaurants(value)
            }}
            onSubmitEditing={this._handleSubmitFilter}
            value={this.state.restaurantsFilter}
            keyboardType='default'
            placeholder='Search'
            style={{
              color: '#000'
            }}
          />
          <RestaurantsListView
            loading={this.props.restaurantsData.loading}
            restaurants={this.state.filteredRestaurants} />
        </ScrollView>
      </View>
    )
  }

  _handleFilterRestaurants = (filter) => {
    filter = filter.toLowerCase()
    let filteredRestaurants = this.state.restaurants.filter(restaurant =>
      restaurant.name.toLowerCase().indexOf(filter) > -1)
    this.setState({
      filteredRestaurants: filteredRestaurants
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15
  }
})
