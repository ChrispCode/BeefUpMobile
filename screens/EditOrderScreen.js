/**
 * @providesModule EditOrderScreen
 * @flow
 */
import React from 'react'
import { ScrollView, StyleSheet, ToastAndroid, View, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import Colors from 'Colors'
import Alerts from 'Alerts'
import { RegularText, LightText } from 'StyledText'
import Actions from 'Actions'
import TouchableNativeFeedbackSafe from '@exponent/react-native-touchable-native-feedback-safe'
import StyledTextInput from 'StyledTextInput'
import OrderItemsListView from 'OrderItemsListView'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'

const query = gql`
    query($id: Int!) {
        restaurant(id: $id) {
            name
            items {
                id
                name
                price
                time_to_prepare
                ingredients
                liquid
                weight
            }
        }
    }
`

const mutation = gql`
    mutation ($id: Int!, $items: [OrderItemInput]!) {
        editOrder(id: $id, items: $items) {
            id
        }
    }
`

@connect(data => OrderScreen.getDataProps)
@compose(
  graphql(query, {
    name: 'restaurantData',
    options: (ownProps) => ({
      variables: {
        id: ownProps.restaurant_id
      }
    })
  }),
  graphql(mutation, {
    props: ({ ownProps, mutate }) => ({
      editOrder: ({ id, items }) => {
        return new Promise(resolve => {
          return resolve(mutate({
            variables: {
              id,
              items
            }
          }))
        })
      }
    })
  })
)
export default class OrderScreen extends React.Component {
  constructor (props, context) {
    super(props, context)

    this.state = {
      itemFilter: '',
      restaurant: null,
      filteredItems: null,
      loading: true,
      orderItems: new Map()
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      restaurant: nextProps.restaurantData.restaurant,
      loading: nextProps.restaurantData.loading
    })
    if (nextProps.restaurantData.restaurant && nextProps.restaurantData.restaurant.items) {
      let orderItemsMap = this.state.orderItems
      let orderItems = new Map()
      this.props.order.items.map(item => {
        let currentItem = Object.assign({}, item.item, {
          state: item.state
        })
        orderItems.set(item.item.id, currentItem)
        orderItemsMap.set(item.item.id, item.quantity)
      })

      let tempItems = []
      nextProps.restaurantData.restaurant.items.map(item => {
        let i = Object.assign({}, item)
        if (!this.state.orderItems.get(item.id)) {
          i.info = {}
          i.info.quantity = 0
        } else {
          i = Object.assign({}, orderItems.get(item.id))
          i.info = {
            quantity: this.state.orderItems.get(item.id)
          }
        }
        tempItems.push(i)
      })

      this.setState({
        filteredItems: tempItems
      })
    }
  }

  static getDataProps (data) {
    return {
      currentUser: data.currentUser,
      pusher: data.pusher
    }
  }

  static route = {
    navigationBar: {
      title () {
        return 'Edit an order'
      }
    }
  };

  render () {
    if (this.state.loading) {
      return (
        <View style={styles.loadingContainer}>
          <ActivityIndicator color={Colors.midGrey} />
          <LightText>Loading...</LightText>
        </View>
      )
    }

    return (
      <View style={{
        flex: 1
      }}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
          automaticallyAdjustContentInsets={false}>

          <StyledTextInput
            autoCorrect={false}
            autoCapitalize='none'
            blurOnSubmit={false}
            onChangeText={value => {
              this.setState({ itemFilter: value })
              this._handleFilterItems(value)
            }}
            onSubmitEditing={this._handleSubmitFilter}
            value={this.state.itemFilter}
            keyboardType='default'
            placeholder='Search'
            style={{
              color: '#000'
            }}
          />
          <OrderItemsListView items={this.state.filteredItems}
            reduceItemQuantity={id => this._handleReduceItemQuantity(id)}
            increaseItemQuantity={id => this._handleIncreaseItemQuantity(id)}
          />
        </ScrollView>
        <View style={styles.footer}>
          <TouchableNativeFeedbackSafe
            background={TouchableNativeFeedbackSafe.Ripple()}
            delayPressIn={0}
            onPress={() => {
              this._handleCreateOrder()
            }}
            style={styles.cancelReservationButton}>
            <RegularText style={styles.cancelReservationButtonText}>Done</RegularText>
          </TouchableNativeFeedbackSafe>
        </View>
      </View>
    );
  }

  _handleCreateOrder () {
    this.props.dispatch(Actions.showGlobalLoading())
    let items = []
    this.state.orderItems.forEach((value, key) => {
      items.push({
        item_id: key,
        quantity: value
      })
    })
    this.props.editOrder({
      id: this.props.order.id,
      items
    })
      .then(data => {
        this.props.onEditOrder()
        this.props.navigator.popToTop()
        ToastAndroid.show('Your order was successfully edited', ToastAndroid.LONG)
      })
      .catch(err => {
        this.props.navigator.showLocalAlert(err.graphQLErrors[0].message, Alerts.error)
      })
      .then(() => {
        this.props.dispatch(Actions.hideGlobalLoading())
      })
  }

  _handleReduceItemQuantity (id) {
    let orderItems = this.state.orderItems
    let item = orderItems.get(id)
    let quantity = 0
    if (item) {
      if (item > 0) {
        orderItems.set(id, item - 1)
        quantity = item - 1
      }
    }
    this.setState({
      orderItems
    })
    let items = [...this.state.filteredItems]
    items.map(item => {
      if (item.id === id) {
        item.info.quantity = quantity
      }
    })
    this.setState({
      filteredItems: items
    })
  }

  _handleIncreaseItemQuantity (id) {
    let orderItems = this.state.orderItems
    let item = orderItems.get(id)
    let quantity
    if (!item) {
      orderItems.set(id, 1)
      quantity = 1
    } else {
      orderItems.set(id, item + 1)
      quantity = item + 1
    }
    this.setState({
      orderItems
    })
    let items = [...this.state.filteredItems]
    items.map(item => {
      if (item.id === id) {
        item.info.quantity = quantity
      }
    })
    this.setState({
      filteredItems: items
    })
  }

  _handleFilterItems = (filter) => {
    filter = filter.toLowerCase()
    let tempItems = []
    this.state.restaurant.items
      .filter(item => item.name.toLowerCase().indexOf(filter) > -1)
      .map(item => {
        let i = Object.assign({}, item)
        i.info = {}
        if (!this.state.orderItems.get(item.id)) {
          i.info.quantity = 0
        } else {
          i.info.quantity = this.state.orderItems.get(item.id)
        }
        tempItems.push(i)
      })
    this.setState({
      filteredItems: tempItems
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  loadingContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelReservationButtonText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: Colors.tintColor
  },
  cancelReservationButton: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 2,
    backgroundColor: '#f7f5f5',
    alignItems: 'center',
    justifyContent: 'center'
  },
  footer: {
    position: 'absolute',
    flex: 1,
    left: 0,
    right: 0,
    bottom: -10,
    paddingHorizontal: 10,
    backgroundColor: '#fff',
    flexDirection: 'row',
    height: 80,
    alignItems: 'center'
  },
  contentContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15
  }
})
