/**
 * @providesModule AuthenticationScreen
 * @flow
 */
import React from 'react'
import {
  ActivityIndicator,
  Image,
  LayoutAnimation,
  ScrollView,
  StyleSheet,
  TextInput,
  View
} from 'react-native'
import TouchableNativeFeedbackSafe from '@exponent/react-native-touchable-native-feedback-safe'
import Actions from 'Actions'
import Alerts from 'Alerts'
import Colors from 'Colors'
import KeyboardEventListener from 'KeyboardEventListener'
import StyledTextInput from 'StyledTextInput'
import { Components } from 'exponent'
import { RegularText } from 'StyledText'
import { connect } from 'react-redux'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'

const mutation = gql`
    mutation ($username: String!, $password: String!) {
        createToken(username: $username, password: $password) {
            token
            role
            id
            restaurant_id
        }
    }
`

@connect()
@compose(
  graphql(mutation, {
    props: ({ ownProps, mutate }) => ({
      createToken: ({ username, password }) => {
        return new Promise(resolve => {
          return resolve(mutate({
            variables: {
              username,
              password
            }
          }))
        })
      }
    })
  })
)
export default class AuthenticationScreen extends React.Component {
  static route = {
    navigationBar: {
      visible: false
    }
  };

  state = {
    username: null,
    password: null,
    isRequestInFlight: false,
    keyboardHeight: 0
  };

  componentWillMount () {
    this._unsubscribe = KeyboardEventListener.subscribe(this._onKeyboardVisibilityChange);
  }

  componentWillUnmount () {
    if (this._unsubscribe) {
      this._unsubscribe()
      this._unsubscribe = null
    }
  }

  render () {
    return (
      <Components.LinearGradient style={{ flex: 1 }} colors={['#8E0AC2', Colors.tintColor]}>
        <ScrollView
          onScroll={this._blurFocusedTextInput}
          scrollEventThrottle={32}
          keyboardDismissMode='on-drag'
          keyboardShouldPersistTaps
          style={styles.container}
          contentContainerStyle={styles.contentContainer}>

          <View style={{ flex: 1, alignItems: 'center' }}>
            {this._maybeRenderLogo()}
          </View>
          <StyledTextInput
            autoCorrect={false}
            autoCapitalize='none'
            blurOnSubmit={false}
            onChangeText={value => {
              this.setState({ username: value.trim() })
            }}
            onSubmitEditing={this._handleSubmitUsername}
            value={this.state.usrename}
            keyboardType='default'
            placeholder='Username'
          />

          <StyledTextInput
            ref={view => {
              this._passwordInput = view
            }}
            onChangeText={value => {
              this.setState({ password: value.trim() })
            }}
            onSubmitEditing={this._handleSignInAsync}
            blurOnSubmit={false}
            value={this.state.password}
            secureTextEntry
            placeholder='Password'
          />

          {this._renderSignInButton()}
        </ScrollView>
      </Components.LinearGradient>
    )
  }

  _maybeRenderLogo () {
    if (this._isKeyboardOpen()) {
      return
    }

    return (
      <Image
        source={require('../assets/images/logo-white.png')}
        style={styles.logoImage}
      />
    )
  }

  _renderSignInButton () {
    let { isRequestInFlight } = this.state
    let loadingIndicator
    if (isRequestInFlight) {
      loadingIndicator = (
        <View style={styles.loadingIndicatorContainer}>
          <ActivityIndicator size='small' color={Colors.midGrey} />
        </View>
      )
    }

    return (
      <View style={{ marginTop: 10 }}>
        <TouchableNativeFeedbackSafe
          background={TouchableNativeFeedbackSafe.Ripple()}
          delayPressIn={0}
          onPress={this._handleSignIn}
          style={styles.signInButton}>
          <RegularText style={styles.signInText}>Sign in</RegularText>
        </TouchableNativeFeedbackSafe>
        {loadingIndicator}
      </View>
    )
  }

  _handleSubmitUsername = () => {
    this._passwordInput && this._passwordInput.focus()
  }

  _handleSignIn = () => {
    let { isRequestInFlight, username, password } = this.state

    if (isRequestInFlight) {
      return
    } else if (!username || !password) {
      return
    }

    this.setState({isRequestInFlight: true})

    this.props.createToken({
      username,
      password
    })
      .then(data => {
        data = data.data
        this._handleSuccessfulLogin({
          token: data.createToken.token,
          role: data.createToken.role,
          id: data.createToken.id,
          restaurant_id: data.createToken.restaurant_id
        })
      })
      .catch(err => {
        this.props.navigator.showLocalAlert(err.graphQLErrors[0].message, Alerts.error)
        this.setState({ isRequestInFlight: false })
      })
  }

  _handleSuccessfulLogin = (result) => {
    this._blurFocusedTextInput()
    this.props.navigator.hideLocalAlert()
    this.props.dispatch(Actions.signIn(result))
  }

  _blurFocusedTextInput = () => {
    TextInput.State.blurTextInput(TextInput.State.currentlyFocusedField())
  };

  _isKeyboardOpen () {
    return this.state.keyboardHeight > 0
  }

  _onKeyboardVisibilityChange = ({ keyboardHeight, layoutAnimationConfig }) => {
    if (keyboardHeight === 0) {
      this._blurFocusedTextInput()
    }

    if (layoutAnimationConfig) {
      LayoutAnimation.configureNext(layoutAnimationConfig)
    }

    this.setState({keyboardHeight})
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20
  },
  logoImage: {
    width: 368 / 2.0,
    height: 232 / 2.0,
    marginBottom: 30
  },
  signInButton: {
    paddingVertical: 10,
    borderRadius: 2,
    backgroundColor: '#f7f5f5',
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingIndicatorContainer: {
    position: 'absolute',
    right: 20,
    paddingTop: 2,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  signInText: {
    backgroundColor: 'transparent',
    fontSize: 18,
    color: Colors.tintColor
  },
  contentContainer: {
    paddingTop: 70,
    paddingHorizontal: 40
  }
})
