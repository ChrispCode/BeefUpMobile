/**
 * @providesModule ActionTypes
 * @flow
 */

export default defineActionConstants([
  'HIDE_GLOBAL_LOADING',
  'SET_CURRENT_USER',
  'SHOW_GLOBAL_LOADING',
  'SIGN_IN',
  'SIGN_OUT',
  'INIT_PUSHER',
  'REMOVE_PUSHER',
  'CREATE_ORDER',
  'PAY_ORDER',
  'AUTHORIZE_CLIENT',
  'QUIT_RESTAURANT'
])

function defineActionConstants (names) {
  return names.reduce((result, name) => {
    result[name] = name
    return result
  }, {})
}
