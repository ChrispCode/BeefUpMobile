/**
 * @providesModule CurrentUserReducer
 */
import ActionTypes from 'ActionTypes'

class CurrentUserReducer {
  static reduce (state = {}, action) {
    if (CurrentUserReducer[action.type]) {
      return CurrentUserReducer[action.type](state, action);
    } else {
      return state
    }
  }

  static [ActionTypes.SIGN_IN] (state, action) {
    return {
      token: action.payload.token,
      role: action.payload.role,
      id: action.payload.id,
      restaurant_id: action.payload.restaurant_id
    }
  }

  // static [ActionTypes.SIGN_OUT] (state, action) {
  //   return {}
  // }

  static [ActionTypes.AUTHORIZE_CLIENT] (state, action) {
    return Object.assign({}, state, {
      restaurant_id: action.payload.restaurant_id
    })
  }

  static [ActionTypes.QUIT_RESTAURANT] (state, action) {
    return Object.assign({}, state, {
      restaurant_id: null
    })
  }

  static [ActionTypes.RESET] (state, action) {
    return {}
  }
}

export default CurrentUserReducer.reduce
