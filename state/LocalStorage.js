/**
 * @providesModule LocalStorage
 * @flow
 */

import { AsyncStorage } from 'react-native'

const Keys = {
  User: 'BeefUpUser',
  History: 'BeefUpHistory'
}

async function getUserAsync () {
  let results = await AsyncStorage.getItem(Keys.User)

  try {
    let user = JSON.parse(results)
    return user
  } catch (e) {
    return null
  }
}

async function saveUserAsync (user) {
  return AsyncStorage.setItem(Keys.User, JSON.stringify(user))
}

async function removeUserAsync () {
  return AsyncStorage.removeItem(Keys.User)
}

async function clearAll () {
  return AsyncStorage.clear()
}

export default {
  saveUserAsync,
  getUserAsync,
  removeUserAsync,
  clearAll
}
