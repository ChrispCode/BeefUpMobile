/**
 * @providesModule BeefUpStore
 * @flow
 */
import { applyMiddleware, combineReducers, createStore } from 'redux'
import { effectsMiddleware } from 'redux-effex'
import ApolloClient, { createNetworkInterface } from 'apollo-client'
import CurrentUserReducer from 'CurrentUserReducer'
import GlobalReducer from 'GlobalReducer'
import PusherReducer from 'PusherReducer'
import Effects from 'Effects'

const serverUri = 'https://graphql.beefup.eu'
const networkInterface = createNetworkInterface({
  uri: serverUri
})

let store

networkInterface.use([{
  applyMiddleware (req, next) {
    let state = store.getState()
    let user = state.currentUser
    if (typeof user.token !== 'undefined') {
      if (!req.options.headers) {
        req.options.headers = {}
      }
      req.options.headers.authorization = user.token
    }

    next()
  }
}])

export const client = new ApolloClient({
  networkInterface
})

store = createStore(
  combineReducers({
    apollo: client.reducer(),
    currentUser: CurrentUserReducer,
    global: GlobalReducer,
    pusher: PusherReducer
  }),
  applyMiddleware(effectsMiddleware(Effects)),
)

export default store
