/**
 * @providesModule Actions
 * @flow
 */
import ActionTypes from 'ActionTypes'

export default class Actions {
  static setCurrentUser (user) {
    return {
      type: ActionTypes.SET_CURRENT_USER,
      payload: {
        ...user
      }
    }
  }

  static signIn (user) {
    return {
      type: ActionTypes.SIGN_IN,
      payload: {
        ...user
      }
    }
  }

  static signOut () {
    return {
      type: ActionTypes.SIGN_OUT
    }
  }

  static initPusher (token) {
    return {
      type: ActionTypes.INIT_PUSHER,
      payload: {
        token
      }
    }
  }

  static removePusher () {
    return {
      type: ActionTypes.REMOVE_PUSHER
    }
  }

  static authorizeUserInRestaurant (restaurant_id) {
    return {
      type: ActionTypes.AUTHORIZE_CLIENT,
      payload: {
        restaurant_id
      }
    }
  }

  static quitRestaurant () {
    return {
      type: ActionTypes.QUIT_RESTAURANT
    }
  }

  static showGlobalLoading () {
    return {
      type: ActionTypes.SHOW_GLOBAL_LOADING
    }
  }

  static hideGlobalLoading () {
    return {
      type: ActionTypes.HIDE_GLOBAL_LOADING
    }
  }
}
