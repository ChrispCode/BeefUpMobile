/**
 * @providesModule PusherReducer
 */
import ActionTypes from 'ActionTypes'
import Pusher from 'pusher-js/react-native'
Pusher.logToConsole = true
let pusher

const configPusher = token => {
  pusher = new Pusher('a681e36f9266dd233601', {
    cluster: 'eu',
    encrypted: true,
    authEndpoint: 'https://graphql.beefup.eu/pusher/auth',
    auth: {
      headers: {
        authorization: token
      }
    }
  });
};

class PusherReducer {
  static reduce (state = {}, action) {
    if (PusherReducer[action.type]) {
      return PusherReducer[action.type](state, action)
    } else {
      return state
    }
  }

  static [ActionTypes.INIT_PUSHER] (state, action) {
    configPusher(action.payload.token)
    return pusher
  }

  static [ActionTypes.REMOVE_PUSHER](state, action) {
    pusher.disconnect()
    pusher = null
    return {}
  }
}

export default PusherReducer.reduce
