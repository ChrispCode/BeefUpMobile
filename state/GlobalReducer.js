/**
 * @providesModule GlobalReducer
 */
import ActionTypes from 'ActionTypes';

class GlobalReducer {
  static reduce (state = {
    isLoading: false
  }, action) {
    if (GlobalReducer[action.type]) {
      return GlobalReducer[action.type](state, action)
    } else {
      return state
    }
  }

  static [ActionTypes.SHOW_GLOBAL_LOADING] (state, action) {
    return Object.assign({}, state, {
      isLoading: true
    })
  }

  static [ActionTypes.HIDE_GLOBAL_LOADING](state, action) {
    return Object.assign({}, state, {
      isLoading: false
    })
  }
}

export default GlobalReducer.reduce
