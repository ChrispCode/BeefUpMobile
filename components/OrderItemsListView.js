/**
 * @providesModule OrderItemsListView
 * @flow
 */
import React from 'react'
import { ActivityIndicator, StyleSheet, View } from 'react-native'
import OrderItemListView from 'OrderItemListView'
import { withNavigation } from '@exponent/ex-navigation'
import Colors from 'Colors'
import { BoldText } from 'StyledText'

@withNavigation
export default class OrderItemsListView extends React.Component {
  state = {
    data: [],
    hasError: false,
    isInitialLoadComplete: false,
    isLoadingApp: false,
    isRefreshing: false,
    isRequestInFlight: false,
    page: 1
  }

  componentDidMount () {
    this.setState({
      isInitialLoadComplete: true
    })
  }

  render () {
    if (this.props.loading) {
      return this._renderLoading()
    } else {
      if (!!this.props.items && this.props.items.length > 0) {
        return (
          <OrderItemListView
            {...this.props}
            isRefreshing={this.state.isRefreshing}
          />
        )
      } else {
        return this._renderNoResults()
      }
    }
  }

  _renderLoading () {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator color={Colors.midGrey} />
      </View>
    )
  }

  _renderNoResults () {
    return (
      <View>
        <BoldText>No items found</BoldText>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingBottom: 80,
    alignItems: 'center',
    justifyContent: 'center'
  }
})
