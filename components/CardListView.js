/**
 * @providesModule CardListView
 * @flow
 */
import React from 'react'
import {
  ActivityIndicator,
  AppState,
  ListView,
  NativeModules,
  PixelRatio,
  RefreshControl,
  Image,
  StyleSheet,
  TouchableHighlight,
  View
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigation } from '@exponent/ex-navigation'
import validPrice from '../utilities/validPrice'
import Colors from 'Colors'
import Layout from 'Layout'
import { RegularText, BoldText } from 'StyledText'
import Router from 'Router'

@connect()
@withNavigation
export default class CardListView extends React.Component {
  constructor (props, context) {
    super(props, context)

    let dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => true
    })

    if (props.cards) {
      dataSource = dataSource.cloneWithRows(props.cards)
    }

    this.state = {
      dataSource,
      selectedCardId: null
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.selectedCardId) {
      this.setState({
        selectedCardId: nextProps.selectedCardId,
        dataSource: this.state.dataSource.cloneWithRows(nextProps.cards)
      })
    }
    if (nextProps.cards !== this.props.cards) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(nextProps.cards)
      })
    }
  }

  render () {
    if (this.state.dataSource.getRowCount() > 0) {
      return this._renderCardList()
    } else {
      return this._renderNoResults()
    }
  }

  _renderCardList () {
    return (
      <View style={{ flex: 1 }}>
        <ListView
          style={{ flex: 1 }}
          contentContainerStyle={{ backgroundColor: '#fff' }}
          dataSource={this.state.dataSource}
          renderRow={this._renderCard}
        />
      </View>
    )
  }

  _renderCard = (card) => {
    let selected = card.id === this.state.selectedCardId

    return (
      <TouchableHighlight
        disabled={!this.props.onSelectCard}
        underlayColor={Colors.veryLightGrey}
        onPress={() => this.props.onSelectCard(card)}>
        <View style={[styles.cardContainer, { backgroundColor: selected ? '#f2f2f2' : '#fff' }]}>
          {this._renderCardIcon(card)}
          {this._renderCardCode(card)}
        </View>
      </TouchableHighlight>
    )
  }

  _renderCardCode (card) {
    let code = 'XXXX XXXX XXXX ' + card.last4
    return (
      <RegularText>{code}</RegularText>
    )
  }

  _renderCardIcon (card) {
    if (card.scheme === 'visa') {
      return <Image
        style={styles.cardIcon}
        source={require('../assets/images/visa.png')}
      />
    } else {
      return <Image
        style={styles.cardIcon}
        source={require('../assets/images/mastercard.png')}
      />
    }
  }

  _renderNoResults () {
    return (
      <View>
        <RegularText>No cards found</RegularText>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  cardIcon: {
    height: 20,
    width: 32
  },
  cardContainer: {
    overflow: 'hidden',
    flexDirection: 'row',
    flex: 1,
    paddingHorizontal: 5,
    paddingTop: 10,
    paddingBottom: 8,
    borderBottomWidth: 3 / PixelRatio.get(),
    borderBottomColor: '#eee',
    justifyContent: 'space-between'
  }
})
