/**
 * @providesModule Floorplan
 * @flow
 */
import React from 'react'
import { Modal,
  WebView, View } from 'react-native'
import { connect } from 'react-redux'
import { RegularText } from 'StyledText'
import createInvoke from 'react-native-webview-invoke/native'

@connect()
export default class Floorplan extends React.Component {
  constructor (props, context) {
    super(props, context)

    this.state = {
      hall: this.props.hall,
      tables: this.props.tables,
      selectedId: this.props.selectedId
    }
  }

  webview: WebView
  invoke = createInvoke(() => this.webview)
  webInitialize = () => {

    this.invoke.fn.set({
      tables: this.state.tables,
      selectedId: this.state.selectedId
    })
  }

  componentDidMount () {
    this.invoke
      .define('init', this.webInitialize)
  }

  render () {
    return (
      <Modal
        animationType={'slide'}
        transparent={false}
        visible={this.props.visible}
        onRequestClose={this.props.onRequestClose}>
        <View style={{ flex: 1 }}>
          <View style={{ paddingVertical: 15,
            paddingHorizontal: 15 }}>
            <RegularText style={{ fontSize: 28 }}>Hall - {this.state.hall.name}</RegularText>
          </View>
          <WebView
            ref={webview => this.webview = webview}
            onMessage={this.invoke.listener}
            source={{uri: 'http://services.beefup.eu/floorplan/webview'}}
          />
        </View>
      </Modal>
    )
  }
}
