/**
 * @providesModule CardsListView
 * @flow
 */
import React from 'react'
import { ActivityIndicator, Image, StyleSheet, TouchableOpacity, View } from 'react-native'
import { withNavigation } from '@exponent/ex-navigation'
import CardListView from 'CardListView'
import Colors from 'Colors'
import { RegularText, BoldText } from 'StyledText'

@withNavigation
export default class CardsListView extends React.Component {
  render () {
    if (this.props.loading) {
      return this._renderLoading()
    } else {
      if (!!this.props.cards && this.props.cards.length > 0) {
        return (
          <CardListView
            onSelectCard={this.props.onSelectCard}
            selectedCardId={this.props.selectedCardId}
            cards={this.props.cards}
          />
        )
      } else {
        return this._renderNoResults()
      }
    }
  }

  _renderLoading () {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator color={Colors.midGrey} />
      </View>
    )
  }

  _renderNoResults () {
    return (
      <View>
        <BoldText>No cards found</BoldText>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  retryButtonWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 40,
  },
  retryButtonHighlight: {
    overflow: 'hidden',
  },
  retryButtonView: {
    borderRadius: 7,
    height: 40,
    backgroundColor: Colors.tintColor,
    justifyContent: 'center',
  },
  retryButtonText: {
    paddingLeft: 20,
    paddingRight: 20,
    color: 'white',
    textAlign: 'center',
    fontWeight: '700'
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingBottom: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
  networkErrorImage: {
    width: 300,
    height: 267,
    opacity: 0.9,
    marginBottom: 30,
  },
});
