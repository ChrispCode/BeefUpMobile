/**
 * @providesModule OrderListView
 * @flow
 */
import React from 'react'
import {
  ActivityIndicator,
  AppState,
  ListView,
  NativeModules,
  PixelRatio,
  RefreshControl,
  StyleSheet,
  TouchableHighlight,
  View
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigation } from '@exponent/ex-navigation'
import validPrice from '../utilities/validPrice'
import Colors from 'Colors'
import Layout from 'Layout'
import { RegularText, BoldText } from 'StyledText'
import Router from 'Router'

@connect()
@withNavigation
export default class OrderListView extends React.Component {
  constructor (props, context) {
    super(props, context)

    let dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2
    })

    if (props.orders) {
      dataSource = dataSource.cloneWithRows(props.orders)
    }

    this.state = {
      isLoadingOrder: false,
      dataSource,
      currentOrder: null,
      openModal: false
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.orders !== this.props.orders) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(nextProps.orders)
      })
    }
  }

  componentDidMount () {
    AppState.addEventListener('change', this._handleAppStateChange)
  }

  render () {
    if (this.state.dataSource.getRowCount() > 0) {
      return this._renderOrderList()
    } else {
      return this._renderNoResults()
    }
  }

  _renderOrderList () {
    return (
      <View style={{ flex: 1 }}>
        <ListView
          style={{ flex: 1 }}
          contentContainerStyle={{ backgroundColor: '#fff' }}
          dataSource={this.state.dataSource}
          renderRow={this._renderOrder}
          refreshControl={
            this.props.onRefresh && (
              <RefreshControl
                refreshing={this.props.isRefreshing}
                onRefresh={this.props.onRefresh}
              />
            )
          }
          onEndReachedThreshold={1200}
          onEndReached={this.props.onEndReached}
        />
        {this.state.isLoadingOrder && this._renderLoading()}
      </View>
    )
  }

  _renderOrder = (order) => {
    let price = 0
    order.items.map(item => {
      price += (item.item.price * item.quantity)
    })
    let disabled = !order.reservation && order.payed

    return (
      <TouchableHighlight
        disabled={disabled}
        underlayColor={Colors.veryLightGrey}
        onPress={() => this._selectOrder(order)}>
        <View style={styles.orderContainer}>
          {!order.payed && <BoldText>Total: {validPrice(price)}</BoldText>}
          {order.payed && <RegularText>Total: {validPrice(price)}</RegularText>}
          <RegularText>{order.items.length} items</RegularText>
        </View>
      </TouchableHighlight>
    )
  }

  _selectOrder (order) {
    this.props.navigation.getNavigator('root').push(Router.getRoute('payOrder', {
      onEditOrder: this.props.onEditOrder,
      cards: this.props.cards,
      order,
      items: order.items
    }))
  }

  _handleAppStateChange = (appState) => {
    if (this.state.isLoadingApp && appState === 'active') {
      this.setState({isLoadingApp: false})
      AppState.removeEventListener('change', this._handleAppStateChange)
    }
  }

  _renderLoading () {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator color={Colors.midGrey} />
      </View>
    );
  }

  _renderNoResults () {
    return (
      <View>
        <RegularText>No orders found</RegularText>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingBottom: 80,
    alignItems: 'center',
    justifyContent: 'center'
  },
  orderContainer: {
    overflow: 'hidden',
    flexDirection: 'row',
    flex: 1,
    paddingTop: 10,
    paddingBottom: 8,
    borderBottomWidth: 3 / PixelRatio.get(),
    borderBottomColor: '#eee',
    justifyContent: 'space-between'
  }
})
