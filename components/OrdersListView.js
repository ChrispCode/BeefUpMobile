/**
 * @providesModule OrdersListView
 * @flow
 */
import React from 'react'
import { ActivityIndicator, Image, StyleSheet, TouchableOpacity, View } from 'react-native'
import { withNavigation } from '@exponent/ex-navigation'
import OrderListView from 'OrderListView'
import Colors from 'Colors'
import { RegularText, BoldText } from 'StyledText'

@withNavigation
export default class OrdersListView extends React.Component {
  state = {
    data: [],
    hasError: false,
    isInitialLoadComplete: false,
    isLoadingApp: false,
    isRefreshing: false,
    isRequestInFlight: false,
    page: 1
  };

  componentDidMount () {
    this.setState({
      isInitialLoadComplete: true
    })
  }

  render () {
    if (this.props.loading) {
      return this._renderLoading()
    } else {
      if (!!this.props.orders && this.props.orders.length > 0) {
        return (
          <OrderListView
            onEditOrder={this.props.onEditOrder}
            cards={this.props.cards}
            orders={this.props.orders}
            hidePayed={this.props.hidePayed}
            isRefreshing={this.state.isRefreshing}
          />
        )
      } else {
        return this._renderNoResults()
      }
    }
  }

  _renderLoading () {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator color={Colors.midGrey} />
      </View>
    )
  }

  _renderNoResults () {
    return (
      <View>
        <BoldText>No orders found</BoldText>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  retryButtonWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 40,
  },
  retryButtonHighlight: {
    overflow: 'hidden',
  },
  retryButtonView: {
    borderRadius: 7,
    height: 40,
    backgroundColor: Colors.tintColor,
    justifyContent: 'center',
  },
  retryButtonText: {
    paddingLeft: 20,
    paddingRight: 20,
    color: 'white',
    textAlign: 'center',
    fontWeight: '700'
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingBottom: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
  networkErrorImage: {
    width: 300,
    height: 267,
    opacity: 0.9,
    marginBottom: 30,
  },
});
