/**
 * @providesModule RestaurantListView
 * @flow
 */
import React from 'react';
import {
  ActionSheetIOS,
  ActivityIndicator,
  AppState,
  Image,
  ListView,
  NativeModules,
  PixelRatio,
  RefreshControl,
  StyleSheet,
  TouchableHighlight,
  View
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigation } from '@exponent/ex-navigation'
import Colors from 'Colors'
import Layout from 'Layout'
import { RegularText, BoldText } from 'StyledText'
import Router from 'Router'
const { ExponentUtil } = NativeModules

@connect()
@withNavigation
export default class RestaurantListView extends React.Component {
  constructor (props, context) {
    super(props, context)

    let dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    })

    if (props.restaurants) {
      dataSource = dataSource.cloneWithRows(props.restaurants)
    }

    this.state = {
      isLoadingRestaurant: false,
      dataSource
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.restaurants !== this.props.restaurants) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(nextProps.restaurants)
      })
    }
  }

  componentDidMount () {
    AppState.addEventListener('change', this._handleAppStateChange)
  }

  render () {
    if (this.state.dataSource.getRowCount() > 0) {
      return this._renderRestaurantList()
    } else {
      return this._renderNoResults()
    }
  }

  _renderRestaurantList () {
    return (
      <View style={{ flex: 1 }}>
        <ListView
          style={{ flex: 1 }}
          contentContainerStyle={{ backgroundColor: '#fff' }}
          dataSource={this.state.dataSource}
          renderRow={this._renderRestaurant}
          initialPageSize={10}
          pageSize={5}
          refreshControl={
            this.props.onRefresh && (
              <RefreshControl
                refreshing={this.props.isRefreshing}
                onRefresh={this.props.onRefresh}
              />
            )
          }
          onEndReachedThreshold={1200}
          onEndReached={this.props.onEndReached}
        />
        {this.state.isLoadingRestaurant && this._renderLoading()}
      </View>
    )
  }

  _shareRestaurant (restaurant) {
    const url = 'https://beefup.eu'
    const message = `Visit ${restaurant.name} on BeefUp`

    if (ExponentUtil && ExponentUtil.shareAsync) {
      ExponentUtil.shareAsync('Share this restaurant', message, url)
    } else {
      ActionSheetIOS.showShareActionSheetWithOptions(
        { url, message },
        (error) => console.log(error),
        (success) => console.log(success)
      )
    }
  }

  _renderRestaurant = (restaurant) => {
    return (
      <TouchableHighlight
        underlayColor={Colors.veryLightGrey}
        onLongPress={() => this._shareRestaurant(restaurant)}
        onPress={() => this._selectRestaurant(restaurant)}>
        <View style={styles.restaurantContainer}>
          <Image
            style={styles.restaurantImage}
            source={require('../assets/images/restaurant-logo.png')}
          />
          <View style={styles.restaurantDescription}>
            <BoldText style={styles.restaurantName}>{restaurant.name}</BoldText>
            <RegularText>{`${restaurant.city}, ${restaurant.country}`}</RegularText>
          </View>
        </View>
      </TouchableHighlight>
    );
  };

  _selectRestaurant (restaurant) {
    this.props.navigation.getNavigator('root').push(Router.getRoute('restaurant', { restaurant }))
  }

  _handleAppStateChange = (appState) => {
    if (this.state.isLoadingApp && appState === 'active') {
      this.setState({ isLoadingApp: false })
      AppState.removeEventListener('change', this._handleAppStateChange)
    }
  }

  _renderLoading () {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator color={Colors.midGrey} />
      </View>
    )
  }

  _renderNoResults () {
    return (
      <View>
        <RegularText>No restaurants found</RegularText>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingBottom: 80,
    alignItems: 'center',
    justifyContent: 'center'
  },
  restaurantContainer: {
    overflow: 'hidden',
    flexDirection: 'row',
    flex: 1,
    paddingTop: 10,
    paddingBottom: 8,
    borderBottomWidth: 3 / PixelRatio.get(),
    borderBottomColor: '#eee'
  },
  restaurantImage: {
    width: 256 / 4.0,
    height: 256 / 4.0
  },
  restaurantName: {
    fontSize: 18
  },
  restaurantDescription: {
    flexDirection: 'column',
    marginLeft: 10
  }
})
