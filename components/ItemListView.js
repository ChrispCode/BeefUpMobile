/**
 * @providesModule ItemListView
 * @flow
 */
import React from 'react'
import {
  Image,
  ListView,
  NativeModules,
  PixelRatio,
  StyleSheet,
  View
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigation } from '@exponent/ex-navigation'
import validPrice from '../utilities/validPrice'
import Colors from 'Colors'
import Layout from 'Layout'
import { RegularText, BoldText } from 'StyledText'

@connect()
@withNavigation
export default class ItemListView extends React.Component {
  constructor (props, context) {
    super(props, context)

    let dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2
    })

    if (props.items) {
      dataSource = dataSource.cloneWithRows(props.items)
    }

    this.state = {
      dataSource
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.items !== this.props.items) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(nextProps.items)
      })
    }
  }

  render () {
    if (this.state.dataSource.getRowCount() > 0) {
      return this._renderItemList()
    } else {
      return this._renderNoResults()
    }
  }

  _renderItemList () {
    return (
      <View style={{ flex: 1 }}>
        <ListView
          style={{ flex: 1 }}
          horizontal={true}
          contentContainerStyle={{ backgroundColor: '#fff' }}
          dataSource={this.state.dataSource}
          renderRow={this._renderItem}
        />
      </View>
    )
  }

  _renderItem = (item) => {
    return (
      <View style={styles.itemContainer}>
        {this._renderImageItem(item)}
        <BoldText>
          {item.name}
        </BoldText>
        <RegularText>{validPrice(item.price)}</RegularText>
      </View>
    )
  }

  _renderImageItem (item) {
    if (item.liquid) {
      return (
        <Image
          style={styles.itemImage}
          source={require('../assets/images/drink.png')}
        />
      )
    }

    return (
      <Image
        style={styles.itemImage}
        source={require('../assets/images/meal.png')}
      />
    )
  }

  _renderNoResults = (item) => {
    return (
      <View>
        <BoldText>No items</BoldText>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 8,
    flexDirection: 'column',
    alignItems: 'center',
    width: 150,
    height: 200,
    borderColor: '#ccc',
    borderWidth: 1,
    marginHorizontal: 2,
    borderRadius: 5
  },
  itemImage: {
    width: 256 / 2.0,
    height: 256 / 2.0
  }
})
