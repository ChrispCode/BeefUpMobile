/**
 * @providesModule ScannerView
 * @flow
 */
import React from 'react'
import { connect } from 'react-redux'
import { Text, View, StyleSheet, Alert } from 'react-native'
import { Components, Permissions } from 'exponent'
import Actions from 'Actions'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'

const mutation = gql`
    mutation($client_id: Int!) {
        authorizeClient(client_id: $client_id) {
            id
        }
    }
`;

@connect(data => ScannerView.getDataProps)
@compose(
  graphql(mutation, {
    props: ({ ownProps, mutate }) => ({
      authorizeClient: ({ client_id }) => {
        return new Promise(resolve => {
          return resolve(mutate({
            variables: {
              client_id
            }
          }))
        })
      }
    })
  })
)
export default class ScannerView extends React.Component {
  state = {
    hasCameraPermission: null,
    isRequestInFlight: false
  }

  static getDataProps (data) {
    return {
      pusher: data.pusher
    }
  }

  async componentWillMount () {
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    this.setState({ hasCameraPermission: status === 'granted' })
  }

  render () {
    const { hasCameraPermission } = this.state
    if (typeof hasCameraPermission === 'null') {
      return <View />
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Components.BarCodeScanner
            onBarCodeRead={this._handleBarCodeRead}
            style={StyleSheet.absoluteFill}
          />
        </View>
      )
    }
  }

  _handleBarCodeRead = (data) => {
    if (!this.state.isRequestInFlight) {
      data = JSON.parse(data.data)
      this.setState({
        isRequestInFlight: true
      })
      this.props.dispatch(Actions.showGlobalLoading())
      this.props.authorizeClient({
        client_id: data.client_id
      })
        .then(() => {
          this.props.dispatch(Actions.hideGlobalLoading())
          Alert.alert(
            'Success!',
            'User is authorized and can continue',
            [
              {
                text: 'OK', onPress: () => {
                  this.setState({
                    isRequestInFlight: false
                  })
                }
              }
            ]
          )
        })
        .catch(err => {
          this.props.dispatch(Actions.hideGlobalLoading())
          Alert.alert(
            'Failed!',
            err.graphQLErrors[0].message,
            [
              {
                text: 'OK', onPress: () => {
                  this.setState({
                    isRequestInFlight: false
                  })
                }
              }
            ]
          )
        })
    }
  }
}
