/**
 * @providesModule ReservationListView
 * @flow
 */
import React from 'react'
import {
  ActivityIndicator,
  AppState,
  ListView,
  NativeModules,
  PixelRatio,
  RefreshControl,
  StyleSheet,
  TouchableHighlight,
  View
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigation } from '@exponent/ex-navigation'
import validDate from '../utilities/validDate'
import Colors from 'Colors'
import Layout from 'Layout'
import { RegularText, BoldText } from 'StyledText'
import Router from 'Router'

@connect()
@withNavigation
export default class OrderListView extends React.Component {
  constructor (props, context) {
    super(props, context)

    let dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2
    })

    if (props.reservations) {
      dataSource = dataSource.cloneWithRows(props.reservations)
    }

    this.state = {
      isLoadingOrder: false,
      dataSource,
      currentOrder: null,
      openModal: false
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.reservations !== this.props.reservations) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(nextProps.reservations)
      })
    }
  }

  render () {
    if (this.state.dataSource.getRowCount() > 0) {
      return this._renderReservationList()
    } else {
      return this._renderNoResults()
    }
  }

  _renderReservationList () {
    return (
      <View style={{ flex: 1 }}>
        <ListView
          style={{ flex: 1 }}
          contentContainerStyle={{ backgroundColor: '#fff' }}
          dataSource={this.state.dataSource}
          renderRow={this._renderReservation}
          refreshControl={
            this.props.onRefresh && (
              <RefreshControl
                refreshing={this.props.isRefreshing}
                onRefresh={this.props.onRefresh}
              />
            )
          }
          onEndReachedThreshold={1200}
          onEndReached={this.props.onEndReached}
        />
        {this.state.isLoadingOrder && this._renderLoading()}
      </View>
    )
  }

  _renderReservation = (reservation) => {
    let restaurantName = reservation.table.hall.restaurant.name
    return (
      <TouchableHighlight
        underlayColor={Colors.veryLightGrey}
        onPress={() => this._selectReservation(reservation)}>
        <View style={styles.reservationContainer}>
          <RegularText>{ validDate(reservation.date) } - { reservation.number_of_people } people</RegularText>
          <RegularText>{ restaurantName }</RegularText>
        </View>
      </TouchableHighlight>
    )
  }

  _selectReservation (reservation) {
    this.props.navigation.getNavigator('root').push(Router.getRoute('reservation', {
      reservation,
      onCancel: this.props.onCancel
    }))
  }

  _renderLoading () {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator color={Colors.midGrey} />
      </View>
    )
  }

  _renderNoResults () {
    return (
      <View>
        <RegularText>No reservations found</RegularText>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingBottom: 80,
    alignItems: 'center',
    justifyContent: 'center'
  },
  reservationContainer: {
    overflow: 'hidden',
    flexDirection: 'row',
    flex: 1,
    paddingTop: 10,
    paddingBottom: 8,
    borderBottomWidth: 3 / PixelRatio.get(),
    borderBottomColor: '#eee',
    justifyContent: 'space-between'
  }
})
