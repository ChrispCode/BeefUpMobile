/**
 * @providesModule OrderItemListView
 * @flow
 */
import React from 'react'
import {
  ActivityIndicator,
  AppState,
  Image,
  ListView,
  NativeModules,
  PixelRatio,
  RefreshControl,
  StyleSheet,
  View
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigation } from '@exponent/ex-navigation'
import TouchableNativeFeedbackSafe from '@exponent/react-native-touchable-native-feedback-safe'
import validPrice from '../utilities/validPrice'
import Colors from 'Colors'
import Layout from 'Layout'
import { RegularText, BoldText, LightText } from 'StyledText'
const { ExponentUtil } = NativeModules
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures'

@connect()
@withNavigation
export default class OrderItemListView extends React.Component {
  constructor (props, context) {
    super(props, context)

    let dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => true
    })

    if (props.items) {
      dataSource = dataSource.cloneWithRows(props.items)
    }

    this.state = {
      isLoadingItem: false,
      dataSource
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(this.props.items)
    })
    // if (nextProps.items !== this.props.items) {
    //   console.log(nextProps.items, this.props.items)
    //   this.setState({
    //     dataSource: this.state.dataSource.cloneWithRows(nextProps.items)
    //   })
    // }
  }

  render () {
    if (this.state.dataSource.getRowCount() > 0) {
      return this._renderItemList()
    } else {
      return this._renderNoResults()
    }
  }

  _renderItemList () {
    return (
      <View style={{ flex: 1 }}>
        <ListView
          style={{ flex: 1 }}
          contentContainerStyle={{ backgroundColor: '#fff' }}
          dataSource={this.state.dataSource}
          renderRow={this._renderItem}
          refreshControl={
            this.props.onRefresh && (
              <RefreshControl
                refreshing={this.props.isRefreshing}
                onRefresh={this.props.onRefresh}
              />
            )
          }
          onEndReachedThreshold={1200}
          onEndReached={this.props.onEndReached}
        />
      </View>
    )
  }

  _renderItem = (item) => {
    if (!item.state) {
      item.state = 'todo'
    }
    let itemColor = item.state !== 'todo' ? item.state === 'done' ? '#30cb00' : '#f7d708' : '#000'

    return (
        <GestureRecognizer
          onSwipe={(direction, state) => {
            if (item.state === 'todo') {
              if (state.moveY < state.y0) {
                this.props.increaseItemQuantity(item.id)
              } else {
                this.props.reduceItemQuantity(item.id)
              }
            }
          }}
          style={{
            flexDirection: 'row'
          }}>
        <View style={styles.itemContainer}>
          <Image
            style={styles.itemImage}
            source={require('../assets/images/meal.png')}
          />
          <View style={styles.itemDescription}>
            <BoldText>{item.name} - {validPrice(item.price)}</BoldText>
            {this._renderIngredients(item.ingredients)}
          </View>
        </View>
        <View style={{
          flexDirection: 'column',
          justifyContent: 'center'
        }}>
          <BoldText style={[styles.quantity, {color: itemColor}]}>{item.info.quantity}</BoldText>
        </View>
        </GestureRecognizer>
    )
  }

  _renderIngredients (ingredients) {
    let toShow = ''
    if (ingredients) {
      ingredients.map(ingredient => {
        toShow += ingredient + ', '
      })
      toShow = toShow.substring(0, toShow.length - 2)
    }
    return (
      <RegularText>{toShow}</RegularText>
    )
  }

  _renderLoading () {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator color={Colors.midGrey} />
      </View>
    );
  }

  _renderNoResults () {
    return (
      <View>
        <RegularText>No items found</RegularText>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  itemContainer: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 8,
    flexDirection: 'row',
    height: 75,
    marginHorizontal: 2,
    borderRadius: 5
  },
  itemImage: {
    width: 256 / 4.0,
    height: 256 / 4.0
  },
  quantity: {
    fontSize: 24,
    marginRight: 15
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingBottom: 80,
    alignItems: 'center',
    justifyContent: 'center'
  },
  itemDescription: {
    flexDirection: 'column',
    flex: 1,
    marginLeft: 10
  },
  infoButton: {
    paddingVertical: 3,
    paddingHorizontal: 7,
    marginRight: 10,
    borderRadius: 2,
    backgroundColor: '#f7f5f5',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
