/**
 * @providesModule RestaurantOrdersView
 * @flow
 */
import React from 'react'
import { connect } from 'react-redux'
import { ScrollView, StyleSheet, View } from 'react-native'
import _ from 'lodash'
import { graphql, compose } from 'react-apollo'
import Colors from 'Colors'
import OrdersListView from 'OrdersListView'
import gql from 'graphql-tag'

const query = gql`
    query($id: Int!) {
        restaurant(id: $id) {
            orders {
                id
                payed
                items {
                    item {
                        id
                        price
                        name
                    }
                    quantity
                }
            }
        }
    }
`

@connect(data => RestaurantOrdersView.getDataProps)
@compose(
  graphql(query, {
    name: 'ordersData',
    options: ({ currentUser }) => ({
      variables: {
        id: currentUser.restaurant_id
      }
    })
  })
)
export default class RestaurantOrdersView extends React.Component {
  static getDataProps (data) {
    return {
      currentUser: data.currentUser,
      pusher: data.pusher
    }
  }

  state = {
    channel: null
  }

  componentWillReceiveProps (nextProps) {
    if (!_.isEmpty(nextProps.pusher) && this.state.channel === null) {
      let channelName = `private-restaurants-${this.props.currentUser.restaurant_id}`
      let channel = nextProps.pusher.subscribe(channelName)
      this.setState({
        channel
      })
      channel.bind('new_order', data => {
        this.props.ordersData.refetch()
      })
    }
  }

  componentWilUnmount () {
    this.setState({
      channel: null
    })
  }

  render () {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
          automaticallyAdjustContentInsets={false}>
          <OrdersListView loading={this.props.ordersData.loading}
            orders={this.props.ordersData.restaurant ? this.props.ordersData.restaurant.orders : []}
            hidePayed
          />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15,
  }
})
