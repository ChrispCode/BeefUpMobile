/**
 * @providesModule signInAsync
 * @flow
 */
import Actions from 'Actions'
import LocalStorage from 'LocalStorage'

export default async function signInAsync ({ action, dispatch }) {
  let user = action.payload
  await LocalStorage.saveUserAsync(user)
  if (typeof user.token !== 'undefined') {
    dispatch(Actions.initPusher(user.token))
  }
}
