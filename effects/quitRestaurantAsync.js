/**
 * @providesModule quitRestaurantAsync
 * @flow
 */
import LocalStorage from 'LocalStorage'

export default async function quitRestaurantAsync ({ action, dispatch }) {
  let user = await LocalStorage.getUserAsync()
  let deauthorizedUser = Object.assign({}, user, {
    restaurant_id: null
  })
  await LocalStorage.saveUserAsync(deauthorizedUser)
}
