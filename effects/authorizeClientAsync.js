/**
 * @providesModule authorizeClientAsync
 * @flow
 */
import LocalStorage from 'LocalStorage'

export default async function authorizeClientAsync ({ action, dispatch }) {
  let user = await LocalStorage.getUserAsync()
  let authorizedUser = Object.assign({}, user, {
    restaurant_id: action.payload.restaurant_id
  })
  await LocalStorage.saveUserAsync(authorizedUser)
}
