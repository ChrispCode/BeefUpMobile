/**
 * @providesModule Effects
 * @flow
 */
import ActionTypes from 'ActionTypes'
import signInAsync from 'signInAsync'
import signOutAsync from 'signOutAsync'
import authorizeClientAsync from 'authorizeClientAsync'
import quitRestaurantAsync from 'quitRestaurantAsync'

function genericErrorHandler({ action, error }) {
  console.log({ error, action })
}

export default [
  { action: ActionTypes.SIGN_OUT, effect: signOutAsync, error: genericErrorHandler },
  { action: ActionTypes.SIGN_IN, effect: signInAsync, error: genericErrorHandler },
  { action: ActionTypes.AUTHORIZE_CLIENT, effect: authorizeClientAsync, error: genericErrorHandler },
  { action: ActionTypes.QUIT_RESTAURANT, effect: quitRestaurantAsync, error: genericErrorHandler }
]
