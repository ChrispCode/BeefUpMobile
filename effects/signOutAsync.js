/**
 * @providesModule signOutAsync
 * @flow
 */
import Actions from 'Actions'
import LocalStorage from 'LocalStorage'

export default async function signOutAsync ({ action, dispatch }) {
  await LocalStorage.clearAll()
  dispatch(Actions.signIn(null))
  dispatch(Actions.removePusher())
}
